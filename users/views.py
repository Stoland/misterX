from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.views.generic.edit import FormView
from django.contrib.auth.forms import UserCreationForm


@login_required
def my_view_auth(request):
    return render(request, 'users/user_profile.html')


class RegisterFormView(FormView):
    form_class = UserCreationForm
    success_url = "/accounts/profile/"
    template_name = "registration/registration.html"

    def form_valid(self, form):
        form.save()
        return super(RegisterFormView, self).form_valid(form)


