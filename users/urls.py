from django.urls import path, include
from django.contrib.auth import views as auth_views
from . import views

app_name = 'users'
urlpatterns = [
    path('', views.my_view_auth),
    path('registration/', views.RegisterFormView.as_view())
]