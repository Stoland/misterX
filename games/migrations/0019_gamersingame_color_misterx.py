# Generated by Django 2.0.7 on 2018-08-10 10:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0018_game_color_city_misterx'),
    ]

    operations = [
        migrations.AddField(
            model_name='gamersingame',
            name='color_misterx',
            field=models.TextField(default='yellow'),
        ),
    ]
