# Generated by Django 2.0.2 on 2019-03-08 16:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0024_auto_20190308_1847'),
    ]

    operations = [
        migrations.AddField(
            model_name='gamersingame',
            name='colors',
            field=models.TextField(default="['red', 'yellow', 'green', 'blue', 'purple']"),
        ),
        migrations.AlterField(
            model_name='gamersingame',
            name='role',
            field=models.TextField(default='detectives'),
        ),
    ]
