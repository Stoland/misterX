# Generated by Django 2.0.2 on 2018-04-13 12:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0009_auto_20180412_1917'),
    ]

    operations = [
        migrations.AddField(
            model_name='game',
            name='queue',
            field=models.TextField(default='None'),
        ),
    ]
