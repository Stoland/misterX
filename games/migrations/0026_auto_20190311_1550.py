# Generated by Django 2.1.7 on 2019-03-11 12:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0025_auto_20190308_1923'),
    ]

    operations = [
        migrations.AddField(
            model_name='game',
            name='start_setting_status',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='game',
            name='queue',
            field=models.TextField(default='detectives'),
        ),
    ]
