# Generated by Django 2.1.7 on 2019-03-19 08:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0030_gamersingame_color_misterx'),
    ]

    operations = [
        migrations.AlterField(
            model_name='game',
            name='color_city_misterx',
            field=models.TextField(default='green'),
        ),
    ]
