# Generated by Django 2.1.7 on 2019-03-30 13:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0033_auto_20190329_1916'),
    ]

    operations = [
        migrations.AddField(
            model_name='game',
            name='evidences',
            field=models.TextField(default=''),
        ),
    ]
