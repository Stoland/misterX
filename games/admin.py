from django.contrib import admin

from .models import Game, GamersInGame

'''
class GamersInline(admin.TabularInline):
    model = User
    extra = 3
'''

class GamerInline(admin.TabularInline):
    model = GamersInGame
    extra = 2

class GameAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['game_name']}),
        ('Date information', {'fields': ['create_date', 'finish_date']}),
    ]
    inlines = [GamerInline]

    list_display = ('game_name', 'create_date', 'was_created_recently')
    list_filter = ['create_date']
    search_fields = ['game_name']


admin.site.register(Game, GameAdmin)
