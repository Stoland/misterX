from .models import Game
from . import data
import logging as log


log.basicConfig(
    level='DEBUG',
    format='%(name)s  %(levelname)s  %(filename)s  %(funcName)s  --> %(message)s',#  %(asctime)s
    filename='./misterx.log',
    filemode='a',
)

def get_cities_of_detetctives(game):  # test
    detectives = [int(a) for a in game.get_cities['detectives'].values()]
    return detectives

def validate_free_city(test_city, enemies_city):  # test
    res = not test_city in enemies_city
    return res

def validate_connection(city_out, city_in, ticket):  # test
    """
        city_out, city_in - integer
        ticket - 'car'
    """
    connections1 = {"city1": city_out, "city2": city_in, "type": ticket}
    connections2 = {"city2": city_out, "city1": city_in, "type": ticket}
    res = connections1 in data.connections or connections2 in data.connections
    return res

def validate_step_detective(city_out, city_in, ticket, game):  # test
    """
        from city_out to city_end, it is their numbers
        ticket has range one
        --
        check: free_city, connection, queue
    """
    check_city = validate_free_city(city_in, get_cities_of_detetctives(game))
    check_ticket = validate_connection(city_out, city_in, ticket)
    check_queue = game.queue == 'detectives'
    res = check_city and check_queue and check_ticket
    return res

def validate_step_misterx(city_out, city_in, ticket, game): # test
    """
        from city_out to city_end, it is their numbers
        ticket has range one
        --
        check: connection, queue
    """
    check_ticket = validate_connection(city_out, city_in, ticket)
    check_queue = game.queue == 'misterx'
    res = check_queue and check_ticket
    return res

def validate_tickets(hypot, game, ging, username): # test
    tickets = game.get_tickets[ging.role][username].split(',')
    return hypot == [(i, tickets[i]) for i, _ in hypot] and len(set(hypot)) == len(hypot)

def validate_step_player(city_out, city_in, ticket, game, ging, color): # test
    city_out, city_in = int(city_out), int(city_in)
    if not color in ging.get_used and color in ging.get_colors:
        if ging.role == 'detectives':
            return validate_step_detective(city_out, city_in, ticket, game)
        elif ging.role == 'misterx':
            return validate_step_misterx(city_out, city_in, ticket, game)
    return False

def validate_special_art_secret_step(city_out, city_in, ticket, addition_tickets, game, ging, color): # test
    """
        cities are integer
        ticket - 'car'
        addition_tickets - [(i, 'car_secret'), ...]
    """
    city_out, city_in = int(city_out), int(city_in)
    return color not in ging.get_used and \
            color in ging.get_colors and \
            [a[1].split('_')[1] for a in addition_tickets] == ['secret']*3 and \
            validate_step_misterx(city_out, city_in, ticket, game)

def validate_special_art_double_step(city1, city2, city3, tickets, addition_tickets, game, ging, color): # test
    """
        cities are integer
        tickets - 'car'
        addition_tickets - [(i, 'car_secret'), ...]
    """
    city1, city2, city3 = int(city1), int(city2), int(city3)
    check_color = (not color in ging.get_used) and color in ging.get_colors
    type_ = [a[1].split('_')[1] for a in addition_tickets] == ['double']*3
    check_step = validate_step_misterx(city1, city2, tickets[0], game) and \
            validate_step_misterx(city2, city3, tickets[1], game)
    log.info([check_step,  type_, check_step])
    return type_ and check_step and check_color

def validate_special_art_addition_step(city_out, city_in, ticket, addition_tickets, game, ging, color): 
    """
        cities are integer
        ticket - 'car'
        addition_tickets - [(i, 'car_secret'), ...]
    """
    city_out, city_in = int(city_out), int(city_in)
    return color in ging.get_colors and \
            [a[1].split('_')[1] for a in addition_tickets] == ['addition']*{'plane':5,'train':4,'car':3}[ticket] and \
            validate_step_detective(city_out, city_in, ticket, game) and \
            len(game.get_city_way['misterx']['white']) >= 6

def validate_win_misterx(game): # test
    cities = set(int(c) for c in game.get_city_way['misterx']['white'].split(',') if c != '')
    color = game.color_city_misterx
    end_cities = set(data.start_cities_misterx['finish'][color])
    res = len(cities & end_cities) > 0
    return res

def validate_win_detectives(game): # test
    city_mx = int(game.get_cities['misterx']['white'])
    cities_detectives = set(map(int, game.get_cities['detectives'].values()))
    res = city_mx in cities_detectives
    return res

def validate_del_tickets(del_tickets, game, ging, login): # test
    tickets = game.get_tickets[ging.role][login].split(',')
    res = True
    for t in del_tickets:
        if t not in tickets:
            res = False
            break
        tickets.remove(t)
    return ging.role == game.queue and res

def validate_add_tickets_for_special_art(tickets, game, ging, login):
    return validate_del_tickets(tickets, game, ging, login)
