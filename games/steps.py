from . import validate_action
import logging as log


log.basicConfig(
    level='DEBUG',
    format='%(name)s  %(levelname)s  %(filename)s  %(funcName)s  --> %(message)s',#  %(asctime)s
    filename='./misterx.log',
    filemode='a',
)


def get_type_of_step(drag, addition_tickets, role): # test
    """
        return type of step <-- drag, addition_tickets, role

        output:
            (type, info)
    """
    if addition_tickets == None or addition_tickets == '':
        addition_tickets = []
    if drag == None or drag == '':
        drag = []
    additions = [a[1].split('_')[1] for a in addition_tickets]
    if len(drag) == 1 and len(additions) == 0:
        return 'normal step', None
    elif len(drag) == 1 and additions == ['secret'] * 3 and role == 'misterx':
        return 'secret step', None
    elif len(drag) == 2 and additions == ['double'] * 3 and role == 'misterx':
        return 'double step', None
    elif len(drag) == 2 and additions == ['double'] * 3 + ['secret'] * 3 and role == 'misterx':
        return 'double secret step', None
    elif len(drag) == 2 and additions == ['secret'] * 3 + ['double'] * 3 and role == 'misterx':
        return 'secret double step', None
    elif len(drag) == 0 and role == 'detectives':
        if set(additions) == {'search'} and 3 <= len(additions) <= 5:
            return 'search step', 7 - len(additions)
        if set(additions) == {'addition'} and 3 <= len(additions) <= 5:
            return 'addition step', {5:'plane', 4:'train', 3:'car'}[len(additions)]
    return False, None


def normal_step(city_out, city_in, drag, game, ging, color, username): # test
    data = {'result': False, 'res_city': int(city_out), 'res_tickets': game.get_tickets[ging.role][username]}
    if validate_action.validate_step_player(int(city_out),int(city_in), drag[1].split('_')[0], game, ging, color):
        # make step on server
        game.change_city(color, int(city_in), ging.role)
        game.delete_tickets([drag[0]], ging.role, username)
        game.add_used_ticket(drag[1], ging.role, color)
        ging.add_used(color)
        data = {'result': True, 'res_city': int(city_in), 'res_tickets': game.get_tickets[ging.role][username]}
        log.info(['normal_step', data])
    return data

def secret_step(city_out, city_in, drag, addition_tickets, game, ging, color, username): # test
    data = {'result': False, 'res_city': int(city_out), 'res_tickets': game.get_tickets[ging.role][username]}
    if validate_action.validate_special_art_secret_step(city_out, city_in, drag[1].split('_')[0], addition_tickets, game, ging, color):
        game.change_city(color, int(city_in), ging.role)
        game.delete_tickets([drag[0], *[a[0] for a in addition_tickets]], ging.role, username)
        game.add_used_ticket('secret', ging.role, color)
        ging.add_used(color)
        data = {'result': True, 'res_city': int(city_in), 'res_tickets': game.get_tickets[ging.role][username]}
        log.info(['secret step', data])
    return data

def double_step(city1, city2, city3, drag, addition_tickets, game, ging, color, username): # test
    data = {'result': False, 'res_city': int(city1), 'res_tickets': game.get_tickets[ging.role][username]}
    log.info([city1, city2, city3, drag, addition_tickets, game.get_tickets[ging.role][username]])
    if validate_action.validate_special_art_double_step(city1, city2, city3, list(map(lambda x: x[1].split('_')[0], drag)), addition_tickets, game, ging, color):
        game.change_city(color, int(city2), ging.role)
        game.change_city(color, int(city3), ging.role)
        game.delete_tickets([*[d[0] for d in drag], *[a[0] for a in addition_tickets]], ging.role, username)
        game.add_used_ticket(drag[0][1], ging.role, color)
        game.add_used_ticket(drag[1][1], ging.role, color)
        ging.add_used(color)
        data = {'result': True, 'res_city': int(city3), 'res_tickets': game.get_tickets[ging.role][username]}
        log.info(['double step', data])
    return data

def double_and_secret_step(city1, city2, city3, drag, addition_tickets, _type, game, ging, color, username): # test
    data = {'result': False, 'res_city': int(city1), 'res_tickets': game.get_tickets[ging.role][username]}
    if _type == 'secret double step':
        double_tickets = addition_tickets[3:]
        secret_tickets = addition_tickets[:3]
    elif _type == 'double secret step':
        double_tickets = addition_tickets[:3]
        secret_tickets = addition_tickets[3:]
    log.info([city1, city2, city3, drag, addition_tickets, game.get_tickets[ging.role][username]])
    if validate_action.validate_special_art_double_step(city1, city2, city3, list(map(lambda x: x[1].split('_')[0], drag)), double_tickets, game, ging, color):
        game.change_city(color, int(city2), ging.role)
        game.change_city(color, int(city3), ging.role)
        game.delete_tickets([*[d[0] for d in drag], *[a[0] for a in addition_tickets]], ging.role, username)
        if _type == 'secret double step':
            game.add_used_ticket('secret', ging.role, color)
            game.add_used_ticket(drag[1][1], ging.role, color)
        elif _type == 'double secret step':
            game.add_used_ticket(drag[0][1], ging.role, color)
            game.add_used_ticket('secret', ging.role, color)
        ging.add_used(color)
        data = {'result': True, 'res_city': int(city3), 'res_tickets': game.get_tickets[ging.role][username]}
        log.info(['double step', data])
    return data

def addition_step(city_out, city_in, drag, addition_tickets, game, ging, color, username):
    data = {'result': False, 'res_city': int(city_out), 'res_tickets': game.get_tickets[ging.role][username]}
    if validate_action.validate_special_art_addition_step(int(city_out),int(city_in), drag, addition_tickets, game, ging, color):
        # make step on server
        game.change_city(color, int(city_in), ging.role)
        game.delete_tickets([a[0] for a in addition_tickets], ging.role, username)
        game.add_used_ticket(drag, ging.role, color)
        # ging.add_used(color)
        data = {'result': True, 'res_city': int(city_in), 'res_tickets': game.get_tickets[ging.role][username]}
        log.info(['addition_step', data])
    return data
