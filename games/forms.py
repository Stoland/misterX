from django import forms

from django.utils import timezone
from datetime import timedelta

from .models import Game


class CreateGameForm(forms.Form):
    game_name = forms.CharField()
    max_count_players = forms.IntegerField(initial=2)

    def save(self, form):
        cd = form.cleaned_data
        game = Game.objects.create(game_name=cd['game_name'],
                                   max_count_players=cd['max_count_players'],
                                   finish_date=timezone.now() + timedelta(hours=300),
                                   create_date=timezone.now()
                                   )
        game.save()
