from django.db import models
from django.utils import timezone
from datetime import timedelta
from django.contrib.auth.models import User

import logging as log
from random import randint
from itertools import product

from . import generation

log.basicConfig(
        level='DEBUG',
        #%(name)s   %(asctime)s
        format='%(asctime)s  %(levelname)s  %(filename)s  %(funcName)s  --> %(message)s',
        filename='./misterx.log',
        filemode='a',
        )


class Game(models.Model):
    game_name = models.CharField(max_length=200)
    create_date = models.DateTimeField('date created')
    finish_date = models.DateTimeField('date finished')

    start_setting_status = models.BooleanField(default=False)

    #creator = models.ForeignKey(User, on_delete=models.CASCADE)

    gamers = models.ManyToManyField(User, through='GamersInGame')
    queue = models.TextField(default='misterx')
    #---мусор
    steps_of_misterx = models.TextField(default='None')
    count_steps = models.IntegerField(default='0')
    #--------
    max_count_players = models.IntegerField(default=2)

    color_city_misterx = models.TextField(default='green')

    cities = models.TextField(
            default="{'misterx':{'white':'0'},'detectives':{'green':'0','red':'0','blue':'0','purple':'0','orange':'0'}}")
    tickets = models.TextField(
            default="{'misterx':{'gamer1':''},'detectives':{'gamer2':''}}")

    used_tickets = models.TextField(
            default="{'misterx':{'white':''},'detectives':{'green':'','red':'','blue':'','purple':'','orange':''}}")
    city_way = models.TextField(
            default="{'misterx':{'white':''},'detectives':{'green':'','red':'','blue':'','purple':'','orange':''}}")

    evidences = models.TextField(default='')

    # for special arts
    current_special_arts = models.TextField(default="{'misterx':{'login1':''},'detectives':{'login2':''}}")
    tickets_for_special_arts = models.TextField(default="{'misterx':{'login1':''},'detectives':{'login2':''}}")
    activity_tickets = models.TextField(default="")

    def __str__(self):
        return self.game_name

    @property
    def count_gamers(self):
        return len(self.gamers.all())

    @property
    def count_gamers_misterx(self):
        count = 0
        for g in self.gamers.all():
            ging = GamersInGame.objects.get(game=self, gamer=g)
            if ging.role == 'misterx':
                count += 1
        return count

    @property
    def can_gamers_add(self):
        res = self.count_gamers < self.max_count_players and self.finish_date >= timezone.now()
        return res

    @property
    def are_all_gamers_ready(self):
        lobby = GamersInGame.objects.filter(game=self)
        res = [l.ready for l in lobby]
        res = self.can_gamers_add == False and res == [True for _ in len(res)]
        return res

    @property
    def get_cities(self):  # +
        return dict(eval(self.cities))

    @property
    def get_tickets(self): # +
        return dict(eval(self.tickets))

    @property
    def get_used_tickets(self): # +
        return dict(eval(self.used_tickets))

    @property
    def get_city_way(self):  # +
        return dict(eval(self.city_way))

    @property
    def get_steps_of_misterx(self): # +
        steps = [s for s in self.get_used_tickets['misterx']['white'].split(',')]
        if 'None' in steps or '' in steps:
            steps = steps[1:]
        return steps

    @property
    def get_count_steps_of_misterx(self):  # +
        return len(self.get_steps_of_misterx)

    @property
    def get_evidences(self):
        return self.evidences.split(',')

    @property
    def get_current_special_arts(self): # +
        return dict(eval(self.current_special_arts))

    @property
    def get_tickets_for_special_arts(self): # +
        log.info(self.tickets_for_special_arts)
        return dict(eval(self.tickets_for_special_arts))

    def add_ticket_for_special_art(self, role, login, ticket): # +
        log.info(self.tickets_for_special_arts)
        tsa = self.get_tickets_for_special_arts
        p = tsa[role][login].split(',')
        p.append(ticket)
        tsa[role][login] = ','.join([c for c in p if c != ''])
        self.tickets_for_special_arts = str(tsa)
        self.save()

    def delete_ticket_for_special_art(self, role, login, ticket): # +
        tsa = self.get_tickets_for_special_arts
        p = tsa[role][login].split(',')
        if ticket in p:
            p.remove(ticket)
        tsa[role][login] = ','.join(p)
        self.tickets_for_special_arts = str(tsa)
        self.save()

    def add_current_special_art(self, role, login, type_art): # +
        csa = self.get_current_special_arts
        p = csa[role][login].split(',')
        p.append(type_art)
        csa[role][login] = ','.join([c for c in p if c != ''])
        self.current_special_arts = str(csa)
        self.save()

    def delete_current_special_art(self, role, login, type_art): # +
        csa = self.get_current_special_arts
        p = csa[role][login].split(',')
        if type_art in p:
            p.remove(type_art)
        csa[role][login] = ','.join(p)
        self.current_special_arts = str(csa)
        self.save()

    def clear_special_arts(self, role, login): # +
        log.info(self.tickets_for_special_arts)
        tickets = self.get_tickets_for_special_arts
        if tickets != '': # if player has special tickets
            self.add_tickets(tickets[role][login].split(','), role, login)
        tickets[role][login] = ''
        self.tickets_for_special_arts = str(tickets)

        csa = self.get_current_special_arts
        csa[role][login] = ''
        self.current_special_arts = str(csa)
        self.save()

    def add_evidences(self): # +
        cities = [str(c) for c in self.get_cities['detectives'].values()]
        mx_way = self.get_city_way['misterx']['white'].split(',')
        f = lambda i, x: x if i >= len(mx_way) - 5 else 0
        mx_way = [f(i, x) for i, x in enumerate(mx_way)]
        evidences = [x.split('+') for x in self.get_evidences if x != '']
        evidences = [[int(x[0]), int(x[1])] for x in evidences]
        for c in cities:
            if c in mx_way:
                step = [mx_way.index(c), int(c)]
                if step not in evidences:
                    evidences.append(step)
        evidences.sort()
        self.evidences = ','.join(map(lambda e: str(e[0]) + '+' + str(e[1]), evidences))
        self.save()
        log.debug([evidences, self.evidences])

    def add_evidence_past(self, count): #+
        city = self.get_city_way['misterx']['white'].split(',')
        evidences = [x.split('+') for x in self.get_evidences if x != '']
        evidences = [[int(x[0]), int(x[1])] for x in evidences]
        new_evi = [len(city) - count-1, int(city[-count-1])]
        if new_evi not in evidences:
            evidences.append(new_evi)
        evidences.sort()
        self.evidences = ','.join(map(lambda e: str(e[0]) + '+' + str(e[1]), evidences))
        self.save()

    def add_used_ticket(self, new_ticket, role='misterx', color='white'):  # +
        used_tickets = self.get_used_tickets
        if used_tickets[role][color] != '':
            used_tickets[role][color] += (',' + str(new_ticket))
        else:
            used_tickets[role][color] += str(new_ticket)
        self.used_tickets = str(used_tickets)
        self.save()
        log.info(self.used_tickets)

    def change_city(self, color, city, role):  # +
        cities = self.get_cities
        city_way = self.get_city_way
        if city_way[role][color] != '':
            city_way[role][color] += (',' + str(city))
        else:
            city_way[role][color] = str(city)

        cities[role][color] = city
        self.cities = str(cities)
        self.city_way = str(city_way)
        self.save()
        log.info([self.cities, self.city_way])

    def change_queue(self):  # +
        if self.queue == 'misterx':
            self.queue = 'detectives'
        else:
            self.queue = 'misterx'
        self.save()

    def add_tickets(self, new_tickets, role, username):
        ''' new_tickets -example- ['car_secret', ...] '''
        all_tickets = self.get_tickets
        tickets = all_tickets[role][username].split(',')
        log.info(['new_tickets', new_tickets, 'tickets', tickets, 'all', all_tickets])
        if new_tickets != [] and new_tickets != ['']:
            for i in range(len(tickets)):
                if tickets[i] == '':
                    tickets[i] = new_tickets[0]
                    del new_tickets[0]
            if len(new_tickets) >= 0:
                tickets += new_tickets

            res = ''
            for t in tickets:
                res += (t + ',')
            res = res[:-1]

            all_tickets[role][username] = res
            self.tickets = str(all_tickets)
            self.save()

    def delete_tickets(self, numbers, role='misterx', username=''):
        all_tickets = self.get_tickets
        tickets = all_tickets[role][username].split(',')
        for n in numbers:
            tickets[n] = ''

        res = ''
        for t in tickets:
            res += (t + ',')
        res = res[:-1]

        all_tickets[role][username] = res
        self.tickets = str(all_tickets)
        self.save()

    def add_gamer(self, gamer):  # +
        if len(self.gamers.filter(id=gamer.id)) == 0:
            GamersInGame.objects.create(game=self, gamer=gamer)
        return self
    # --------------------


    def choice_misterx(self): #+
        tickets = {'misterx': {}, 'detectives':{}}

        if self.max_count_players == 2:
            detective_colors = (['red', 'orange', 'green', 'blue', 'purple'], [])
        elif self.max_count_players == 3:
            detective_colors = (['red', 'orange', 'green'], ['blue', 'purple'])
        elif self.max_count_players == 4:
            detective_colors = (['red', 'orange'], ['green', 'blue'], ['purple'])
        elif self.max_count_players == 5:
            detective_colors = (['red', 'orange'], ['green'], ['blue'], ['purple'])
        else:
            detective_colors = (['red'], ['orange'], ['green'], ['blue'], ['purple'])
        detective_colors = iter(detective_colors)

        if self.count_gamers_misterx == 0:
            misterx = randint(0, self.count_gamers-1)
            players = self.gamers.all()
        else:
            misterx = randint(0, self.count_gamers_misterx-1)
            players = [player for player in self.gamers.all()
                    if GamersInGame.objects.get(game=self, gamer=player).role == 'misterx']
            players += [player for player in self.gamers.all() if player not in players]
        for i, gamer in enumerate(players):
            ging = GamersInGame.objects.get(game=self, gamer=gamer)
            if i == misterx:
                ging.role = 'misterx'
                ging.colors = str(['white'])
                tickets['misterx'][str(gamer.username)] = ''
                self.color_city_misterx = ging.color_misterx
            else:
                ging.role = 'detectives'
                ging.colors = str(next(detective_colors))
                tickets['detectives'][str(gamer.username)] = ''
            ging.save()
        self.tickets = str(tickets)
        self.save()

    def fill_tickets(self, player): # +
        ging = GamersInGame.objects.get(game=self, gamer=player)
        count_tickets = sum(1 for ticket in self.get_tickets[ging.role][player.username].split(',') if ticket != '')
        self.add_tickets(generation.generate_new_tickets(ging.get_count_tickets - count_tickets), ging.role, ging.gamer.username)

    def fill_tickets_for_detectives(self): # +
        gings = GamersInGame.objects.filter(game=self)
        players = [g.gamer for g in gings if g.role == 'detectives']
        for p in players:
            self.fill_tickets(p)
        for g in [g for g in gings if g.role == 'detectives']:
            g.clear_used()
            g.save()

    def generate_players_positions(self):
        cities = self.get_cities
        cities['misterx']['white'] = generation.generate_city_for_misterx(self.color_city_misterx)
        cities['detectives'] = {k: c for k, c in zip(cities['detectives'].keys(), generation.generate_cities_for_detectives())}
        self.cities = str(cities)
        self.save()

    def init_tickets_for_special_arts(self):
        res = {'misterx': {}, 'detectives': {}}
        for ging in GamersInGame.objects.filter(game=self):
            res[ging.role][ging.gamer.username] = ''
        self.tickets_for_special_arts = str(res)
        self.save()
        log.info([self.tickets_for_special_arts, self.get_tickets_for_special_arts])

    def initialization(self): # without general test
        self.choice_misterx()

        #fill tickets
        for ging in GamersInGame.objects.filter(game=self):
            self.fill_tickets(ging.gamer)

        # init special tickets
        self.init_tickets_for_special_arts()

        # generate positions
        self.generate_players_positions()

        # save status
        self.start_setting_status = True
        self.save()

    # --------------------
    def was_created_recently(self):
        now = timezone.now()
        return now - timedelta(days=1) <= self.create_date <= now

    was_created_recently.admin_order_field = 'create_date'
    was_created_recently.boolean = True
    was_created_recently.short_description = 'Created recently?'

    class Meta:
        ordering = ['-create_date']


class GamersInGame(models.Model):
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    gamer = models.ForeignKey(User, on_delete=models.CASCADE)
    ready = models.BooleanField(default=False)
    # role_misterx = models.BooleanField(default=False)
    color_misterx = models.TextField(default='green')

    role = models.TextField(default='detectives')
    colors = models.TextField(default="['red', 'orange', 'green', 'blue', 'purple']")
    used = models.TextField(default="")

    def __str__(self):
        return '  game: ' + str(self.game) +\
                ', gamer: ' + str(self.gamer) +\
                ', ready: ' + str(self.ready) +\
                ', colors: ' + str(self.colors) +\
                ', role: ' + str(self.role)

    @property
    def get_colors(self):
        return eval(self.colors)

    @property
    def get_count_tickets(self):
        if self.role == 'misterx':
            return 8
        else:
            return 4 + len(self.get_colors)

    @property
    def get_used(self):
        return set(self.used.split(','))

    def add_used(self, color):
        if self.used == '':
            self.used += color
        else:
            self.used += ',' + str(color)
        self.save()

    def clear_used(self):
        self.used = ""
        self.save()
