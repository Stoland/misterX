from django.urls import path

from . import views

app_name = 'games'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('create/', views.CreateGame.as_view(), name='create_game'),
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('<int:pk>/setup', views.setup_view, name='setup'),
    path('<int:pk>/main', views.open_main, name='main'),
    path('<int:pk>/end_game', views.end_game, name='end_game'),

    path('<int:pk>/ajax/status_gamers/', views.status_gamers, name='status_gamers'),
    path('<int:pk>/ajax/color_misterx/', views.color_misterx, name='color_misterx'),
    path('<int:pk>/ajax/test_city/', views.test_city, name='test_city'),
    path('<int:pk>/ajax/save_city/', views.save_city, name='save_city'),
    path('<int:pk>/ajax/setting_roles/', views.setting_roles, name='setting_roles'),
    path('<int:pk>/ajax/get_role/', views.get_role, name='get_role'),
    path('<int:pk>/ajax/change_queue/', views.change_queue, name='change_queue'),
    path('<int:pk>/ajax/test_queue/', views.test_queue, name='test_queue'),
    path('<int:pk>/ajax/add_step_of_misterx/', views.add_step_of_misterx, name='add_step_of_misterx'),
    path('<int:pk>/ajax/change_detectives_info/', views.change_detectives_info, name='change_detectives_info'),

    path('<int:pk>/ajax/get_steps_of_misterx/', views.get_steps_of_misterx, name='get_steps_of_misterx'),
    path('<int:pk>/ajax/get_tickets/', views.get_tickets, name='get_tickets'),
    path('<int:pk>/ajax/get_queue/', views.get_queue, name='get_queue'),
    path('<int:pk>/ajax/get_positions/', views.get_positions, name='get_positions'),
    path('<int:pk>/ajax/check_move_player/', views.check_move_player, name='check_move_player'),
    path('<int:pk>/ajax/remove_tickets/', views.remove_tickets, name='remove_tickets'),
    path('<int:pk>/ajax/complite_course/', views.complite_course, name='complite_course'),
    path('<int:pk>/ajax/get_evidences/', views.get_evidences, name='get_evidences'),
    path('<int:pk>/ajax/get_color_misterx/', views.get_color_misterx, name='get_color_misterx'),
    path('<int:pk>/ajax/check_end/', views.check_end, name='check_end'),
    path('<int:pk>/ajax/validate_double_step/', views.validate_double_step, name='validate_double_step'),
    path('<int:pk>/ajax/search_step/', views.search_step, name='search_step'),
    path('<int:pk>/ajax/validate_addition_step/', views.validate_addition_step, name='validate_addition_step'),
    path('<int:pk>/ajax/get_colors/', views.get_colors, name='get_colors'),
    path('<int:pk>/ajax/add_special_arts/', views.add_special_arts, name='add_special_arts'),
]
