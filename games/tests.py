from unittest.mock import MagicMock, patch, sentinel
import json
import datetime

from django.test import TestCase, Client
from django.urls import reverse
from django.utils import timezone
from django.http import JsonResponse, QueryDict

from .views import *
from . import views
from .models import Game, GamersInGame
from .forms import CreateGameForm
from django.contrib.auth.models import User

from .validate_action import *
from . import data
from . import validate_action
from . import generation
from . import setting_game
from . import steps


def create_game(game_name, days):
    create_date = timezone.now() + datetime.timedelta(days=days)
    finish_date = create_date + datetime.timedelta(hours=1)
    return Game.objects.create(game_name=game_name,
                               create_date=create_date,
                               finish_date=finish_date)


def create_form():
    game_name = 'Test game'
    create_date = timezone.now()
    finish_date = create_date + datetime.timedelta(hours=1)
    return CreateGameForm.create(game_name=game_name,
                                 create_date=create_date,
                                 finish_date=finish_date)


class GameIndexViewTests(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='login', id=1)
        self.user.set_password('12345')
        self.user.save()
        self.client = Client()
        self.client.login(username='login', password='12345')

    def test_no_games(self):
        response = self.client.get(reverse('games:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "No games are available.")
        self.assertQuerysetEqual(
            response.context['game_list'],
            []
        )

    def test_past_game(self):
        create_game(game_name="Past game.", days=-30)
        response = self.client.get(reverse('games:index'))
        self.assertQuerysetEqual(
            response.context['game_list'],
            ['<Game: Past game.>']
        )

    def test_future_game(self):
        create_game(game_name="Future game.", days=30)
        response = self.client.get(reverse('games:index'))
        self.assertContains(response, "No games are available.")
        self.assertQuerysetEqual(
            response.context['game_list'],
            []
        )

    def test_future_game_and_past_game(self):
        create_game(game_name="Past game.", days=-30)
        create_game(game_name="Future game.", days=30)
        response = self.client.get(reverse('games:index'))
        self.assertQuerysetEqual(
            response.context['game_list'],
            ['<Game: Past game.>']
        )

    def test_two_past_games(self):
        create_game(game_name="Past game 1.", days=-30)
        create_game(game_name="Past game 2.", days=-5)
        response = self.client.get(reverse('games:index'))
        self.assertQuerysetEqual(
            response.context['game_list'],
            ['<Game: Past game 2.>', '<Game: Past game 1.>']
        )


class GameDetailViewTests(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='login', id=1)
        self.user.set_password('12345')
        self.user.save()
        self.client = Client()
        self.client.login(username='login', password='12345')

    def test_future_game(self):
        future_game = create_game(game_name='Future game.', days=5)
        url = reverse('games:detail', args=(future_game.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_past_game(self):
        past_game = create_game(game_name='Past game.', days=-5)
        url = reverse('games:detail', args=(past_game.id,))
        response = self.client.get(url)
        self.assertContains(response, past_game.game_name)


class GameSetupViewTests(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='login', id=1)
        self.user.set_password('12345')
        self.user.save()
        self.client = Client()
        self.client.login(username='login', password='12345')

    def test_future_game(self):
        future_game = create_game(game_name='Future game.', days=5)
        url = reverse('games:setup', args=(future_game.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_past_game(self):
        past_game = create_game(game_name='Past game.', days=-5)
        url = reverse('games:setup', args=(past_game.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_active_game(self):
        active_game = create_game(game_name='Active game.', days=0)
        url = reverse('games:setup', args=(active_game.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['game'], active_game)


class CreateGameViewTest(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='login', id=1)
        self.user.set_password('12345')
        self.user.save()
        self.client = Client()
        self.client.login(username='login', password='12345')

    def test_open_create_game(self):
        url = reverse('games:create_game')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)


class AddGamerToGameTest(TestCase):
    def setUp(self):

        self.client = Client()

        self.user = User.objects.create(username='login', id=1)
        self.user.set_password('12345')
        self.user.save()
        self.client.login(username='login', password='12345')

        self.user2 = User.objects.create(username='login2', id=2)
        self.user2.set_password('12345')
        self.user2.save()
        self.client.login(username='login2', password='12345')

    def test_add_one_gamer(self):
        game = create_game(game_name='test game', days=0)
        GamersInGame.objects.create(game=game, gamer=self.user)
        self.assertEqual(game.count_gamers, 1)
        self.assertEqual(game.gamers.get(
            username=self.user.username), self.user)

    def test_add_two_gamer(self):
        game = create_game(game_name='test game', days=0)
        GamersInGame.objects.create(game=game, gamer=self.user)
        GamersInGame.objects.create(game=game, gamer=self.user2)
        self.assertEqual(game.count_gamers, 2)


class TestGame(TestCase):
    def setUp(self):
        self.user1 = User.objects.create(username='login1', id=1)
        self.user2 = User.objects.create(username='login2', id=2)
        self.user3 = User.objects.create(username='login3', id=3)

    def test_change_one_city(self):
        obj = MagicMock()
        obj.get_cities = {'misterx': {'white': '1'},
                          'detectives': {'green': '2'}}
        obj.get_city_way = {'misterx': {'white': '3,1'},
                            'detectives': {'green': '2'}}
        Game.change_city(obj, 'white', '5', 'misterx')
        self.assertDictEqual({'detectives': {'green': '2'}, 'misterx': {'white': '5'}},
                             eval(obj.cities))
        self.assertDictEqual({'detectives': {'green': '2'}, 'misterx': {'white': '3,1,5'}},
                             eval(obj.city_way))
        obj.save.assert_called_with()

    def test_change_two_city(self):
        obj = MagicMock()
        obj.get_cities = {'misterx': {'white': ''},
                          'detectives': {'green': '1', 'red': '3'}}
        obj.get_city_way = {'misterx': {'white': ''},
                            'detectives': {'green': '1', 'red': '1,4'}}
        Game.change_city(obj, 'white', '5', 'misterx')
        Game.change_city(obj, 'red', '10', 'detectives')
        self.assertDictEqual({'detectives': {'green': '1', 'red': '10'}, 'misterx': {'white': '5'}},
                             eval(obj.cities))
        self.assertDictEqual({'detectives': {'green': '1', 'red': '1,4,10'}, 'misterx': {'white': '5'}},
                             eval(obj.city_way))
        obj.save.assert_called_with()

    def test_one_change_queue(self):
        obj = MagicMock()
        obj.queue = 'detectives'
        Game.change_queue(obj)
        self.assertEqual('misterx', obj.queue)
        obj.save.assert_called_with()

    def test_two_change_queue(self):
        obj = MagicMock()
        obj.queue = 'detectives'

        Game.change_queue(obj)
        Game.change_queue(obj)
        self.assertEqual(obj.queue, 'detectives')
        obj.save.assert_called_with()

    def test_add_one_ticket(self):
        obj = MagicMock()
        obj.used_tickets = "{'misterx':{'white':''}}"
        obj.get_used_tickets = dict(eval(obj.used_tickets))

        Game.add_used_ticket(obj, new_ticket='car_secret')
        self.assertEqual(obj.used_tickets, str(
            {'misterx': {'white': 'car_secret'}}))

    def test_add_three_tickets(self):
        obj = MagicMock()
        obj.used_tickets = "{'misterx':{'white':''}, 'detectives':{'red':'car_secret', 'green':''}}"
        obj.get_used_tickets = dict(eval(obj.used_tickets))

        Game.add_used_ticket(obj, new_ticket='car_secret')
        Game.add_used_ticket(obj, new_ticket='train_secret',
                             role='detectives', color='red')
        Game.add_used_ticket(obj, new_ticket='plane_double',
                             role='detectives', color='green')
        self.assertEqual(obj.get_used_tickets,
                         {'misterx': {'white': 'car_secret'}, 'detectives': {'red': 'car_secret,train_secret', 'green': 'plane_double'}})

    def test_add_tickets_1(self):
        game = create_game('test_game', 0)
        game.tickets = "{'misterx':{'gamer1':''},'detectives':{'green':''}}"
        game.add_tickets(['car_secret', 'plane_search'],
                         role='detectives', username='green')
        self.assertEqual(game.get_tickets['detectives']['green'], 'car_secret,plane_search')

    def test_add_tickets_2(self):
        game = create_game('test_game', 0)
        game.tickets = "{'misterx':{'gamer1':''},'detectives':{'green':'car_double'}}"
        game.add_tickets(['car_secret', 'plane_search'],
                         role='detectives', username='green')
        self.assertEqual(game.get_tickets['detectives']['green'], 'car_double,car_secret,plane_search')

    def test_add_tickets_3(self):
        game = create_game('test game', 0)
        game.tickets = "{'misterx':{'gamer1':''},'detectives':{'green':''}}"
        game.add_tickets(['car_secret', 'plane_search'],
                         role='detectives', username='green')
        game.tickets = "{'misterx':{'gamer1':'car_secret,,,plane_search'}}"
        game.add_tickets(['car_secret', 'car_secret',
                          'train_secret'], role='misterx', username='gamer1')
        self.assertEqual(game.get_tickets['misterx']['gamer1'],
                         'car_secret,car_secret,car_secret,plane_search,train_secret')

    def test_delete_tickets(self):
        game = create_game('test game', 0)
        game.tickets = "{'misterx':{'gamer1':'car_secret,plane_search,train_secret,car_secret'}}"
        game.delete_tickets([1, 3], role='misterx', username='gamer1')
        self.assertEqual(
            game.get_tickets['misterx']['gamer1'], 'car_secret,,train_secret,')

    def choice_misterx_0(self):
        game = create_game('test', 0)
        GamersInGame.objects.create(
            game=game, gamer=self.user1, role='detectives')
        GamersInGame.objects.create(
            game=game, gamer=self.user2, role='detectives')
        GamersInGame.objects.create(
            game=game, gamer=self.user3, role='detectives')

        game.choice_misterx()

        self.assertEqual(sorted([ging.role for ging in [GamersInGame.objects.get(game=game, gamer=login) for login in [self.user1, self.user2, self.user3]]]),
            sorted(['misterx', 'detectives', 'detectives']))
        self.assertEqual(game.color_city_misterx, 'green')

    def test_choice_misterx_1(self):
        game = create_game('test', 0)
        GamersInGame.objects.create(
            game=game, gamer=self.user1, role='misterx')
        GamersInGame.objects.create(
            game=game, gamer=self.user2, role='detectives')
        GamersInGame.objects.create(
            game=game, gamer=self.user3, role='detectives')
        game.choice_misterx()

        self.assertEqual([ging.role for ging in [GamersInGame.objects.get(game=game, gamer=login) for login in [self.user1, self.user2, self.user3]]], ['misterx', 'detectives', 'detectives'])
        self.assertEqual(game.color_city_misterx, 'green')

    def test_choice_misterx_2(self):
        game = create_game('test', 0)
        GamersInGame.objects.create(
            game=game, gamer=self.user1, role='misterx')
        GamersInGame.objects.create(
            game=game, gamer=self.user2, role='misterx')
        GamersInGame.objects.create(
            game=game, gamer=self.user3, role='detectives')
        game.choice_misterx()

        self.assertEqual(sorted([ging.role for ging in [GamersInGame.objects.get(game=game, gamer=login) for login in [self.user1, self.user2]]]), sorted(['detectives', 'misterx'])) 
        self.assertEqual(game.color_city_misterx, 'green')

    def test_choice_misterx_3(self):
        game = create_game('test', 0)
        GamersInGame.objects.create(
                game=game, gamer=self.user1, role='misterx')
        GamersInGame.objects.create(
                game=game, gamer=self.user2, role='misterx')
        GamersInGame.objects.create(
                game=game, gamer=self.user3, role='misterx')
        game.choice_misterx()
        
        self.assertEqual(sorted([ging.role for ging in [GamersInGame.objects.get(game=game, gamer=login) for login in [self.user1, self.user2, self.user3]]]),
                sorted(['detectives', 'detectives', 'misterx']))
        self.assertEqual(game.color_city_misterx, 'green')

    def test_choice_misterx_color(self):
        game = create_game('test', 0)
        GamersInGame.objects.create(
            game=game, gamer=self.user1, role='misterx', color_misterx='orange')
        GamersInGame.objects.create(
            game=game, gamer=self.user2, role='detectives')
        GamersInGame.objects.create(
            game=game, gamer=self.user3, role='detectives')
        game.choice_misterx()

        self.assertEqual(game.color_city_misterx, 'orange')

    def test_choice_misterx_color_default(self):
        game = create_game('test', 0)
        GamersInGame.objects.create(
            game=game, gamer=self.user1, role='misterx')
        GamersInGame.objects.create(
            game=game, gamer=self.user2, role='detectives')
        GamersInGame.objects.create(
            game=game, gamer=self.user3, role='detectives')
        game.choice_misterx()

        self.assertEqual(game.color_city_misterx, 'green')

    @patch('random.randint', return_value=8)
    def test_fill_tickets_0(self, super_randint):
        game = create_game('test game', 0)
        GamersInGame.objects.create(
            game=game, gamer=self.user1, role='detectives')
        game.tickets = "{'misterx':{'gamer1':''},'detectives':{'login1':''}}"

        game.fill_tickets(self.user1)
        self.assertEqual(game.get_tickets['misterx']['gamer1'], '')
        self.assertEqual(game.get_tickets['detectives']['login1'], ('car_double,'*9)[:-1])

    @patch('random.randint', return_value=8)
    def test_fill_tickets_1(self, super_randint):
        game = create_game('test game', 0)
        GamersInGame.objects.create(
            game=game, gamer=self.user1, role='detectives')
        game.tickets = "{'misterx':{'gamer1':''},'detectives':{'login1':'car_secret'}}"

        game.fill_tickets(self.user1)
        self.assertEqual(game.get_tickets['detectives']['login1'], ('car_secret,' + 'car_double,'*8)[:-1])

    @patch('random.randint', return_value=8)
    def test_fill_tickets_2(self, super_randint):
        game = create_game('test game', 0)
        GamersInGame.objects.create(
            game=game, gamer=self.user1, role='detectives')
        game.tickets = "{'misterx':{'gamer1':''},'detectives':{'login1':'car_secret,,train_double,,,'}}"

        game.fill_tickets(self.user1)
        self.assertEqual(game.get_tickets['detectives']['login1'], ('car_secret,car_double,train_double,' + 'car_double,'*6)[:-1])

    @patch('random.randint', return_value=8)
    def test_fill_tickets_for_detectives(self, super_randint):
        game = create_game('test game', 0)
        ging2 = GamersInGame.objects.create(
            game=game, gamer=self.user2, role='detectives')
        ging1 = GamersInGame.objects.create(
            game=game, gamer=self.user1, role='detectives', used='red,blue')
        game.tickets = "{'misterx':{'gamer1':''},'detectives':{'login1':'car_secret,,train_double,,,', 'login2':'car_secret,,train_double,,,'}}"

        game.fill_tickets_for_detectives()
        self.assertEqual(game.get_tickets['detectives']['login1'], ('car_secret,car_double,train_double,' + 'car_double,'*6)[:-1])
        self.assertEqual(game.get_tickets['detectives']['login2'], ('car_secret,car_double,train_double,' + 'car_double,'*6)[:-1])
        self.assertEqual(GamersInGame.objects.get(game=game, gamer=self.user1).used, '')
        self.assertEqual(GamersInGame.objects.get(game=game, gamer=self.user2).used, '')

    @patch('random.randint', side_effect=[0,0,1,2,3,4])
    def test_generate_players_positions(self, super_randint):
        game = create_game('test_game', 0)
        game.cities = "{'misterx':{'white':'0'},'detectives':{'green':'0','red':'0','blue':'0','purple':'0','orange':'0'}}"
        game.save()

        res = {'misterx':{'white':190},'detectives':{'green':41,'red':56,'blue':76,'purple':92,'orange':108}}
        game.generate_players_positions()
        self.assertEqual(game.get_cities, res)

    def test_add_evidences(self):
        game = create_game('test game', 0)
        game.cities = "{'misterx':{'white':'1'},'detectives':{'green':'2','red':'3','blue':'4','purple':'5','orange':'6'}}"
        game.city_way = "{'misterx':{'white':'15,2,5,155,7,3,1'}}"
        game.evidences = "3+155"
        game.save()

        game.add_evidences()
        self.assertEqual("2+5,3+155,5+3", game.evidences)

    def test_add_current_special_art_1(self):
        game = create_game('test game', 0)
        game.current_special_arts = "{'misterx':{'login1':''},'detectives':{'login2':''}}"
        game.save()

        game.add_current_special_art('misterx', 'login1', 'double_step')
        self.assertEqual("double_step", game.get_current_special_arts['misterx']['login1'])

    def test_add_current_special_art_2(self):
        game = create_game('test game', 0)
        game.current_special_arts = "{'misterx':{'login1':'secret_step'},'detectives':{'login2':''}}"
        game.save()

        game.add_current_special_art('misterx', 'login1', 'double_step')
        self.assertEqual("secret_step,double_step", game.get_current_special_arts['misterx']['login1'])

    def test_delete_current_special_art_1(self):
        game = create_game('test game', 0)
        game.current_special_arts = "{'misterx':{'login1':'double_step'},'detectives':{'login2':''}}"
        game.save()

        game.delete_current_special_art('misterx', 'login1', 'double_step')
        self.assertEqual("", game.get_current_special_arts['misterx']['login1'])

    def test_delete_current_special_art_2(self):
        game = create_game('test game', 0)
        game.current_special_arts = "{'misterx':{'login1':'double_step,secret_step'},'detectives':{'login2':''}}"
        game.save()

        game.delete_current_special_art('misterx', 'login1', 'double_step')
        self.assertEqual("secret_step", game.get_current_special_arts['misterx']['login1'])

    def test_delete_current_special_art_3(self):
        game = create_game('test game', 0)
        game.current_special_arts = "{'misterx':{'login1':'secret_step'},'detectives':{'login2':''}}"
        game.save()

        game.delete_current_special_art('misterx', 'login1', 'double_step')
        self.assertEqual("secret_step", game.get_current_special_arts['misterx']['login1'])

    def test_add_ticket_for_special_art_1(self):
        game = create_game('test game', 0)
        game.tickets_for_special_arts = "{'misterx':{'login1':''},'detectives':{'login2':''}}"
        game.save()

        game.add_ticket_for_special_art('misterx', 'login1', 'car_secret')
        self.assertEqual("car_secret", game.get_tickets_for_special_arts['misterx']['login1'])

    def test_add_ticket_for_special_art_2(self):
        game = create_game('test game', 0)
        game.tickets_for_special_arts = "{'misterx':{'login1':'car_secret'},'detectives':{'login2':''}}"
        game.save()

        game.add_ticket_for_special_art('misterx', 'login1', 'car_double')
        self.assertEqual("car_secret,car_double", game.get_tickets_for_special_arts['misterx']['login1'])

    def test_delete_ticket_for_special_art_1(self):
        game = create_game('test game', 0)
        game.tickets_for_special_arts = "{'misterx':{'login1':'car_secret,car_secret,train_secret'},'detectives':{'login2':''}}"
        game.save()

        game.delete_ticket_for_special_art('misterx', 'login1', 'car_secret')
        self.assertEqual("car_secret,train_secret", game.get_tickets_for_special_arts['misterx']['login1'])

    def test_delete_ticket_for_special_art_2(self):
        game = create_game('test game', 0)
        game.tickets_for_special_arts = "{'misterx':{'login1':'car_secret,car_double,plane_double,train_double'},'detectives':{'login2':''}}"
        game.save()

        game.delete_ticket_for_special_art('misterx', 'login1', 'plane_double')
        self.assertEqual("car_secret,car_double,train_double", game.get_tickets_for_special_arts['misterx']['login1'])

    def test_delete_ticket_for_special_art_3(self):
        game = create_game('test game', 0)
        game.tickets_for_special_arts = "{'misterx':{'login1':'car_secret,car_double,plane_double,train_double'},'detectives':{'login2':''}}"
        game.save()

        game.delete_ticket_for_special_art('misterx', 'login1', 'plane_secret')
        self.assertEqual("car_secret,car_double,plane_double,train_double", game.get_tickets_for_special_arts['misterx']['login1'])

    def test_clear_special_arts_1(self):
        game = create_game('test game', 0)
        game.current_special_arts = "{'misterx':{'login1':'secret_step'},'detectives':{'login2':''}}"
        game.tickets_for_special_arts = "{'misterx':{'login1':'car_secret,car_double,plane_double,train_double'},'detectives':{'login2':''}}"
        game.tickets = "{'misterx':{'login1':'train_addition,,plane_secret'},'detectives':{'login2':''}}"
        game.save()

        game.clear_special_arts('misterx', 'login1')
        self.assertEqual('train_addition,car_secret,plane_secret,car_double,plane_double,train_double', game.get_tickets['misterx']['login1'])
        self.assertEqual('', game.get_current_special_arts['misterx']['login1'])

    def test_clear_special_arts_2(self):
        game = create_game('test game', 0)
        game.current_special_arts = "{'misterx':{'login1':'secret_step'},'detectives':{'login2':''}}"
        game.tickets_for_special_arts = "{'misterx':{'login1':''},'detectives':{'login2':''}}"
        game.tickets = "{'misterx':{'login1':'train_addition,,plane_secret'},'detectives':{'login2':''}}"
        game.save()

        game.clear_special_arts('misterx', 'login1')
        self.assertEqual('train_addition,,plane_secret', game.get_tickets['misterx']['login1'])
        self.assertEqual('', game.get_current_special_arts['misterx']['login1'])

    def test_clear_special_arts_3(self):
        game = create_game('test game', 0)
        game.current_special_arts = "{'misterx':{'login1':''},'detectives':{'login2':''}}"
        game.tickets_for_special_arts = "{'misterx':{'login1':''},'detectives':{'login2':''}}"
        game.tickets = "{'misterx':{'login1':'train_addition,plane_secret'},'detectives':{'login2':''}}"
        game.save()

        game.clear_special_arts('misterx', 'login1')
        self.assertEqual('train_addition,plane_secret', game.get_tickets['misterx']['login1'])
        self.assertEqual('', game.get_current_special_arts['misterx']['login1'])

    def test_clear_special_arts_4(self):
        game = create_game('test game', 0)
        game.current_special_arts = "{'misterx':{'login1':'secret_step'},'detectives':{'login2':'abc'}}"
        game.tickets_for_special_arts = "{'misterx':{'login1':'secret_car'},'detectives':{'login2':'double_step'}}"
        game.tickets = "{'misterx':{'login1':'train_addition,,plane_secret'},'detectives':{'login2':'train_secret'}}"
        game.save()

        game.clear_special_arts('misterx', 'login1')
        self.assertEqual('train_addition,secret_car,plane_secret', game.get_tickets['misterx']['login1'])
        self.assertEqual('train_secret', game.get_tickets['detectives']['login2'])
        self.assertEqual('', game.get_current_special_arts['misterx']['login1'])
        self.assertEqual('abc', game.get_current_special_arts['detectives']['login2'])

    def test_init_tickets_for_special_arts_1(self):
        game = create_game('test game', 0)
        GamersInGame.objects.create(game=game, gamer=self.user1, role='misterx')
        GamersInGame.objects.create(game=game, gamer=self.user2, role='detectives')
        game.tickets_for_special_arts = "{'misterx':{'d1':'hfk'},'detectives':{'fha':'fhk'}}"
        
        game.init_tickets_for_special_arts()
        self.assertEqual(game.get_tickets_for_special_arts, {'misterx':{'login1':''},'detectives':{'login2':''}})


class TestPropertyOfGame(TestCase):
    def test_get_count_steps_of_misterx(self):
        game = create_game('test_game', 0)
        game.used_tickets = "{'misterx':{'white':'car_secret,plane_double'}}"
        self.assertEqual(game.get_count_steps_of_misterx, 2)

    def test_get_steps_of_misterx(self):
        game = create_game('test_game', 0)
        game.used_tickets = "{'misterx':{'white':'car_secret,plane_double'}}"
        self.assertEqual(game.get_steps_of_misterx, ['car_secret', 'plane_double'])

    def test_get_cities(self):
        game = create_game('test_game', 0)
        cities = {'misterx': {'white': '5'}, 'detectives': {'green': '4'}}
        game.cities = str(cities)
        self.assertDictEqual(game.get_cities, cities)

    def test_get_tickets(self):
        game = create_game('test_game', 0)
        tickets = {'misterx': {'gamer1': 'car_double,,car_secret'}, 'detectives': {'gamer2': 'car_secret'}}
        game.tickets = str(tickets)
        self.assertDictEqual(game.get_tickets, tickets)

    def test_get_used_tickets(self):
        game = create_game('test_game', 0)
        used_tickets = {'misterx': {'white': '5'},
                        'detectives': {'green': '4'}}
        game.used_tickets = str(used_tickets)
        self.assertDictEqual(game.get_used_tickets, used_tickets)

    def test_get_city_way(self):
        game = create_game('test_game', 0)
        city_way = {'misterx': {'white': '5'}, 'detectives': {'green': '4'}}
        game.city_way = str(city_way)
        self.assertDictEqual(game.get_city_way, city_way)

    def test_get_evidences(self):
        game = create_game('test_game', 0)
        evidences = ['1+2', '3+2', '4+15']
        game.evidences = ','.join(evidences)
        self.assertEqual(game.get_evidences, evidences)

    def test_get_current_special_arts(self):
        game = create_game('test_game', 0)
        game.current_special_arts = "{'misterx':{'login1':'double_step'},'detectives':{'login2':''}}"
        game.save()
        self.assertEqual(game.get_current_special_arts, {'misterx':{'login1':'double_step'},'detectives':{'login2':''}})

    def test_get_tickets_for_special_arts(self):
        game = create_game('test_game', 0)
        game.tickets_for_special_arts = "{'misterx':{'login1':'car_secret,train_secret,train_secret'},'detectives':{'login2':''}}"
        game.save()
        self.assertEqual(game.get_tickets_for_special_arts, {'misterx':{'login1':'car_secret,train_secret,train_secret'},'detectives':{'login2':''}})

    def test_add_evidence_past_1(self):
        game = create_game('test_game', 0)
        game.city_way = "{'misterx':{'white':'1,2,3,4,5,6,7'}}"
        game.evidences = '0+1,3+4'
        game.save()
        game.add_evidence_past(4)
        self.assertEqual(game.evidences, '0+1,2+3,3+4')

    def test_add_evidence_past_2(self):
        game = create_game('test_game', 0)
        game.city_way = "{'misterx':{'white':'1,2,3,4,5,6,7'}}"
        game.evidences = '0+1,3+4'
        game.save()
        game.add_evidence_past(2)
        self.assertEqual(game.evidences, '0+1,3+4,4+5')


class TestGamerInGame(TestCase):
    def setUp(self):
        self.user1 = User.objects.create(username='login1', id=1)
        self.user2 = User.objects.create(username='login2', id=2)
        self.user3 = User.objects.create(username='login3', id=3)
    
    def test_get_colors(self):
        game = create_game('test', 0)
        ging = GamersInGame.objects.create(
            game=game, gamer=self.user1, colors='["r", "y"]')
        self.assertEqual(ging.get_colors, ["r", "y"])


    def test_get_count_tickets_1(self):
        game = create_game('test', 0)
        ging = GamersInGame.objects.create(
            game=game, gamer=self.user1, role='misterx')
        self.assertEqual(ging.get_count_tickets, 8)

    def test_get_count_tickets_2(self):
        game = create_game('test', 0)
        ging = GamersInGame.objects.create(
            game=game, gamer=self.user1, role='detectives')
        self.assertEqual(ging.get_count_tickets, 9)


class TestViews(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='login', id=1)
        self.user.set_password('12345')
        self.user.save()
        self.client = Client()
        self.client.login(username='login', password='12345')

        self.user2 = User.objects.create(username='login2', id=2)
        self.user2.set_password('12345')
        self.user2.save()
        self.client.login(username='login2', password='12345')

    def test_status_gamers_get(self):
        request = MagicMock()
        request.method = 'GET'
        request.user = self.user
        game = create_game('test game', 0)
        ging = GamersInGame.objects.create(game=game, gamer=self.user)
        res = {
            'players': [(self.user.id, ging.ready, ging.role, self.user == request.user)],
            'max_count': 2
        }
        self.assertEqual(status_gamers(request, game.pk).content,
                         JsonResponse(res).content)

    def test_status_gamers_post(self):
        request = MagicMock()
        request.method = 'POST'
        request.user = self.user
        request.POST = QueryDict('status=I-am-ready')
        game = create_game('test game', 0)
        ging = GamersInGame.objects.create(game=game, gamer=self.user)
        res = status_gamers(request, game.pk).content
        self.assertEqual(res, JsonResponse({'success': True, 'role': 'detectives', 'ready': True}).content)

    def test_get_steps_of_misterx_1(self):
        request = MagicMock()
        request.method = 'GET'
        request.user = self.user
        game = create_game('test game', 0)
        ging = GamersInGame.objects.create(game=game, gamer=self.user)
        game.used_tickets = "{'misterx':{'white':''},'detectives':{}}" 
        game.save()
        res = {'steps': [], 'count': 0}

        self.assertEqual(JsonResponse(res).content, get_steps_of_misterx(request, game.pk).content)
    
    def test_get_steps_of_misterx_2(self):
        request = MagicMock()
        request.method = 'GET'
        request.user = self.user
        game = create_game('test game', 0)
        ging = GamersInGame.objects.create(game=game, gamer=self.user)
        game.used_tickets = "{'misterx':{'white':'car_double,car_secret'},'detectives':{}}" 
        game.save()
        res = {'steps': ['car_double', 'car_secret'], 'count': 2}

        self.assertEqual(JsonResponse(res).content, get_steps_of_misterx(request, game.pk).content)

    def test_get_role_1(self):
        request = MagicMock()
        request.method = 'GET'
        request.user = self.user
        game = create_game('test game', 0)
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx')

        self.assertEqual(JsonResponse({'role': 'misterx'}).content, get_role(request, game.pk).content)

    def test_get_role_2(self):
        request = MagicMock()
        request.method = 'GET'
        request.user = self.user
        game = create_game('test game', 0)
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives')

        self.assertEqual(JsonResponse({'role': 'detectives'}).content, get_role(request, game.pk).content)

    def test_get_tickets_1(self):
        request = MagicMock()
        request.method = 'GET'
        request.user = self.user
        game = create_game('test game', 0)
        game.tickets = "{'detectives':{'login':'car_secret,double_train'}}"
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives')

        self.assertEqual(JsonResponse({'tickets': 'car_secret,double_train'}).content, get_tickets(request, game.pk).content)

    def test_get_queue(self):
        request = MagicMock()
        request.method = 'GET'
        request.user = self.user
        game = create_game('test game', 0)
        game.queue = "detectives"
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives')

        self.assertEqual(JsonResponse({'queue': 'detectives'}).content, get_queue(request, game.pk).content)

    def test_get_positions_1(self):
        request = MagicMock()
        request.method = 'GET'
        request.user = self.user
        game = create_game('test game', 0)
        game.cities = "{'misterx':{'white':'1'},'detectives':{'green':'2','red':'3','blue':'4','purple':'5','orange':'6'}}"
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives')

        res = {'green':'2','red':'3','blue':'4','purple':'5','orange':'6'}
        self.assertEqual(JsonResponse(res).content, get_positions(request, game.pk).content)

    def test_get_positions_2(self):
        request = MagicMock()
        request.method = 'GET'
        request.user = self.user
        game = create_game('test game', 0)
        game.cities = "{'misterx':{'white':'1'},'detectives':{'green':'2','red':'3','blue':'4','purple':'5','orange':'6'}}"
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx')

        res = {'white':'1', 'green':'2','red':'3','blue':'4','purple':'5','orange':'6'}
        self.assertEqual(JsonResponse(res).content, get_positions(request, game.pk).content)

    def test_check_step_player_1(self):
        request = MagicMock()
        request.method = 'POST'
        request.user = self.user
        request.POST.get = lambda x, _: {'color': 'green', 'start': 1, 'end': 2, 'drag': '0+car_secret', 'special_arts':''}[x]
        game = create_game('test game step player', 0)
        game.tickets = "{'misterx':{'login2':''}, 'detectives':{'login':'car_secret'}}"
        game.queue = 'detectives'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives')

        res = {'result': True, 'res_city': 2, 'res_tickets':''}
        self.assertEqual(JsonResponse(res).content, check_move_player(request, game.pk).content)

    def test_check_step_player_2(self):
        request = MagicMock()
        request.method = 'POST'
        request.user = self.user
        request.POST.get = lambda x, _: {'color': 'green', 'start': 1, 'end': 2, 'drag': '1+car_secret', 'special_arts':''}[x]
        game = create_game('test game step player', 0)
        game.tickets = "{'misterx':{'login2':''}, 'detectives':{'login':'plane_addition,car_secret,,,train_double', 'login3':''}}"
        game.queue = 'detectives'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives')

        res = {'result': True, 'res_city': 2, 'res_tickets':'plane_addition,,,,train_double'}
        self.assertEqual(JsonResponse(res).content, check_move_player(request, game.pk).content)

    def test_check_step_player_3(self):
        request = MagicMock()
        request.method = 'POST'
        request.user = self.user
        request.POST.get = lambda x, _: {'color': 'green', 'start': 1, 'end': 2, 'drag': '1+car_secret', 'special_arts':''}[x]
        game = create_game('test game step player', 0)
        game.tickets = "{'misterx':{'login2':''}, 'detectives':{'login':'plane_addition,plane_secret,,,train_double'}}"
        game.queue = 'detectives'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives')

        res = {'result': False, 'res_city': 1, 'res_tickets':'plane_addition,plane_secret,,,train_double'}
        self.assertEqual(JsonResponse(res).content, check_move_player(request, game.pk).content)

    def test_check_step_player_4(self):
        request = MagicMock()
        request.method = 'POST'
        request.user = self.user
        request.POST.get = lambda x, _: {'color': 'green', 'start': 1, 'end': 2, 'drag': '1+car_secret,2+car_secret', 'special_arts':''}[x]
        game = create_game('test game step player', 0)
        game.tickets = "{'misterx':{'login2':''}, 'detectives':{'login':'plane_addition,plane_secret,,,train_double'}}"
        game.queue = 'detectives'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives')

        res = {'result': False, 'res_city': 1, 'res_tickets':'plane_addition,plane_secret,,,train_double'}
        self.assertEqual(JsonResponse(res).content, check_move_player(request, game.pk).content)

    def test_check_step_player_5(self):
        request = MagicMock()
        request.method = 'POST'
        request.user = self.user
        request.POST.get = lambda x, _: {'color': 'green', 'start': 1, 'end': 2, 'drag': '1+plane_secret', 'special_arts':''}[x]
        game = create_game('test game step player', 0)
        game.tickets = "{'misterx':{'login2':''}, 'detectives':{'login':'plane_addition,plane_secret,,,train_double'}}"
        game.queue = 'detectives'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives')

        res = {'result': False, 'res_city': 1, 'res_tickets':'plane_addition,plane_secret,,,train_double'}
        self.assertEqual(JsonResponse(res).content, check_move_player(request, game.pk).content)

    def test_check_step_player_6(self):
        request = MagicMock()
        request.method = 'POST'
        request.user = self.user
        request.POST.get = lambda x, _: {'color': 'green', 'start': 1, 'end': 2, 'drag': '1+plane_secret', 'special_arts':''}[x]
        game = create_game('test game step player', 0)
        game.tickets = "{'misterx':{'login2':''}, 'detectives':{'login':'train_addition,plane_addition,,,train_double'}}"
        game.queue = 'detectives'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives')

        res = {'result': False, 'res_city': 1, 'res_tickets':'train_addition,plane_addition,,,train_double'}
        self.assertEqual(JsonResponse(res).content, check_move_player(request, game.pk).content)

    def test_check_step_player_7(self):
        request = MagicMock()
        request.method = 'POST'
        request.user = self.user
        request.POST.get = lambda x, _: {'color': 'green', 'start': 1, 'end': 2, 'drag': '1+plane_secret', 'special_arts':''}[x]
        game = create_game('test game step player', 0)
        game.tickets = "{'misterx':{'login2':''}, 'detectives':{'login':'train_addition,plane_addition,,,train_double'}}"
        game.queue = 'misterx'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives')

        res = {'result': False, 'res_city': 1, 'res_tickets':'train_addition,plane_addition,,,train_double'}
        self.assertEqual(JsonResponse(res).content, check_move_player(request, game.pk).content)

    def test_check_step_player_8(self):
        request = MagicMock()
        request.method = 'POST'
        request.user = self.user
        request.POST.get = lambda x, _: {'color': 'green', 'start': 1, 'end': 2, 'drag': '0+plane_secret', 'special_arts':''}[x]
        game = create_game('test game step player', 0)
        game.tickets = "{'misterx':{'login0':''}, 'detectives':{'login2': 'plane_secret', 'login':'train_addition,plane_addition,,,train_double'}}"
        game.queue = 'detectives'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives')

        res = {'result': False, 'res_city': 1, 'res_tickets':'train_addition,plane_addition,,,train_double'}
        self.assertEqual(JsonResponse(res).content, check_move_player(request, game.pk).content)

    def test_check_step_player_9(self):
        request = MagicMock()
        request.method = 'POST'
        request.user = self.user
        request.POST.get = lambda x, _: {'color': 'green', 'start': 1, 'end': 2, 'drag': '0+car_secret', 'special_arts':''}[x]
        game = create_game('test game step player', 0)
        game.tickets = "{'misterx':{'login0':''}, 'detectives':{'login': 'car_secret', 'login3':'train_addition,plane_addition,,,train_double'}}"
        game.queue = 'detectives'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives')

        res = {'result': True, 'res_city': 2, 'res_tickets':''}
        self.assertEqual(JsonResponse(res).content, check_move_player(request, game.pk).content)

    def test_check_step_player_10(self):
        request = MagicMock()
        request.method = 'POST'
        request.user = self.user
        request.POST.get = lambda x, _: {'color': 'green', 'start': 1, 'end': 2, 'drag': None, 'special_arts':''}[x]
        game = create_game('test game step player', 0)
        game.tickets = "{'misterx':{'login0':''}, 'detectives':{'login': 'car_secret', 'login3':'train_addition,plane_addition,,,train_double'}}"
        game.queue = 'detectives'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives')

        res = {'result': False, 'res_city': 1, 'res_tickets':'car_secret'}
        self.assertEqual(JsonResponse(res).content, check_move_player(request, game.pk).content)

    def test_check_step_player_11(self):
        request = MagicMock()
        request.method = 'POST'
        request.user = self.user
        request.POST.get = lambda x, _: {'color': 'green', 'start': 1, 'end': 2, 'drag': '0+car_secret', 'special_arts':''}[x]
        game = create_game('test game step player', 0)
        game.tickets = "{'misterx':{'login2':''}, 'detectives':{'login':'plane_addition,car_secret,,,train_double', 'login3':''}}"
        game.queue = 'detectives'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives')

        res = {'result': False, 'res_city': 1, 'res_tickets':'plane_addition,car_secret,,,train_double'}
        self.assertEqual(JsonResponse(res).content, check_move_player(request, game.pk).content)

    def test_check_step_player_12(self):
        request = MagicMock()
        request.method = 'POST'
        request.user = self.user
        request.POST.get = lambda x, _: {'color': 'green', 'start': 1, 'end': 2, 'drag': '', 'special_arts':''}[x]
        game = create_game('test game step player', 0)
        game.tickets = "{'misterx':{'login0':''}, 'detectives':{'login': 'car_secret', 'login3':'train_addition,plane_addition,,,train_double'}}"
        game.queue = 'detectives'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives')

        res = {'result': False, 'res_city': 1, 'res_tickets':'car_secret'}
        self.assertEqual(JsonResponse(res).content, check_move_player(request, game.pk).content)

    def test_check_step_player_secret_1(self):
        request = MagicMock()
        request.method = 'POST'
        request.user = self.user
        request.POST.get = lambda x, _: {'color': 'white', 'start': 1, 'end': 2, 'drag': '1+car_secret', 'special_arts':'0+car_secret,2+train_secret,3+car_secret'}[x]
        game = create_game('test game step player', 0)
        game.tickets = "{'misterx':{'login':'car_secret,car_secret,train_secret,car_secret'}, 'detectives':{'login1': 'car_secret'}}"
        game.queue = 'misterx'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx', colors = "['white']")

        res = {'result': True, 'res_city': 2, 'res_tickets':',,,'}
        self.assertEqual(JsonResponse(res).content, check_move_player(request, game.pk).content)

    def test_check_step_player_secret_2(self):
        request = MagicMock()
        request.method = 'POST'
        request.user = self.user
        request.POST.get = lambda x, _: {'color': 'white', 'start': 1, 'end': 2, 'drag': '1+car_secret', 'special_arts':'0+car_double,2+train_secret,3+car_secret'}[x]
        game = create_game('test game step player', 0)
        game.tickets = "{'misterx':{'login':'car_double,car_secret,train_secret,car_secret'}, 'detectives':{'login1': 'car_secret'}}"
        game.queue = 'misterx'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx', colors = "['white']")

        res = {'result': False, 'res_city': 1, 'res_tickets':'car_double,car_secret,train_secret,car_secret'}
        self.assertEqual(JsonResponse(res).content, check_move_player(request, game.pk).content)

    def test_check_step_player_secret_3(self):
        request = MagicMock()
        request.method = 'POST'
        request.user = self.user
        request.POST.get = lambda x, _: {'color': 'white', 'start': 1, 'end': 2, 'drag': '1+car_secret', 'special_arts':'0+car_secret,2+train_secret,3+car_secret,4+plane_secret'}[x]
        game = create_game('test game step player', 0)
        game.tickets = "{'misterx':{'login':'car_secret,car_secret,train_secret,car_secret,plane_secret'}, 'detectives':{'login1': 'car_secret'}}"
        game.queue = 'misterx'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx', colors = "['white']")

        res = {'result': False, 'res_city': 1, 'res_tickets':'car_secret,car_secret,train_secret,car_secret,plane_secret'}
        self.assertEqual(JsonResponse(res).content, check_move_player(request, game.pk).content)

    @patch('games.validate_action.validate_connection', return_value=True)
    def test_check_step_player_double_1(self, super_va):
        request = MagicMock()
        request.method = 'POST'
        request.user = self.user
        request.POST.get = lambda x, _: {'color': 'white', 'city1': 1, 'city2': 2, 'city3': 3, 'drag': '0+car_secret,1+plane_search', 'special_arts':'2+car_double,3+train_double,4+car_double'}[x]
        game = create_game('test game step player', 0)
        game.tickets = "{'misterx':{'login':'car_secret,plane_search,car_double,train_double,car_double'}, 'detectives':{'login1': 'car_double'}}"
        game.queue = 'misterx'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx', colors = "['white']")

        res = {'result': True, 'res_city': 3, 'res_tickets':',,,,'}
        self.assertEqual(JsonResponse(res).content, check_move_player(request, game.pk).content)

    @patch('games.validate_action.validate_connection', return_value=False)
    def test_check_step_player_double_2(self, super_va):
        request = MagicMock()
        request.method = 'POST'
        request.user = self.user
        request.POST.get = lambda x, _: {'color': 'white', 'city1': 1, 'city2': 2, 'city3': 3, 'drag': '0+car_secret,1+plane_search', 'special_arts':'2+car_double,3+train_double,4+car_double'}[x]
        game = create_game('test game step player', 0)
        game.tickets = "{'misterx':{'login':'car_secret,plane_search,car_double,train_double,car_double'}, 'detectives':{'login1': 'car_double'}}"
        game.queue = 'misterx'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx', colors = "['white']")

        res = {'result': False, 'res_city': 1, 'res_tickets':'car_secret,plane_search,car_double,train_double,car_double'}
        self.assertEqual(JsonResponse(res).content, check_move_player(request, game.pk).content)

    def test_check_step_player_double_3(self):
        request = MagicMock()
        request.method = 'POST'
        request.user = self.user
        request.POST.get = lambda x, _: {'color': 'white', 'start': 1, 'end': 2, 'drag': '1+car_double', 'special_arts':'0+car_double,2+train_double,3+car_double,4+plane_double'}[x]
        game = create_game('test game step player', 0)
        game.tickets = "{'misterx':{'login':'car_double,car_double,train_double,car_double,plane_double'}, 'detectives':{'login1': 'car_double'}}"
        game.queue = 'misterx'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx', colors = "['white']")

        res = {'result': False, 'res_city': 1, 'res_tickets':'car_double,car_double,train_double,car_double,plane_double'}
        self.assertEqual(JsonResponse(res).content, check_move_player(request, game.pk).content)

    def test_check_step_player_double_and_secret_1(self):
        request = MagicMock()
        request.method = 'POST'
        request.user = self.user
        request.POST.get = lambda x, _: {'color': 'white', 'city1': 1, 'city2': 2, 'city3': 3, 'drag': '0+car_double,1+car_double', 'special_arts':'2+train_double,3+car_double,4+plane_double,5+car_secret,6+car_secret,7+car_secret'}[x]
        game = create_game('test game step player', 0)
        game.tickets = "{'misterx':{'login':'car_double,car_double,train_double,car_double,plane_double,car_secret,car_secret,car_secret'}, 'detectives':{'login1': 'car_double'}}"
        game.queue = 'misterx'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx', colors = "['white']")

        res = {'result': True, 'res_city': 3, 'res_tickets':',,,,,,,'}
        self.assertEqual(JsonResponse(res).content, check_move_player(request, game.pk).content)

    def test_remove_tickets_1(self):    
        request = MagicMock()
        request.method = 'POST'
        request.user = self.user
        request.POST.get = lambda x, _: {'tickets': '0+car_secret'}[x]
        game = create_game('test game step player', 0)
        game.tickets = "{'misterx':{'login0':''}, 'detectives':{'login': 'car_secret', 'login3':'train_addition,plane_addition,,,train_double'}}"
        game.queue = 'detectives'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives')

        res = {'res_tickets':''}
        self.assertEqual(JsonResponse(res).content, remove_tickets(request, game.pk).content)

    def test_remove_tickets_2(self):    
        request = MagicMock()
        request.method = 'POST'
        request.user = self.user
        request.POST.get = lambda x, _: {'tickets': '0+car_secret'}[x]
        game = create_game('test game step player', 0)
        game.tickets = "{'misterx':{'login0':''}, 'detectives':{'login': 'car_secret', 'login3':'train_addition,plane_addition,,,train_double'}}"
        game.queue = 'misterx'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives')

        res = {'res_tickets':'car_secret'}
        self.assertEqual(JsonResponse(res).content, remove_tickets(request, game.pk).content)

    def test_remove_tickets_3(self):    
        request = MagicMock()
        request.method = 'POST'
        request.user = self.user
        request.POST.get = lambda x, _: {'tickets': '0+car_secret'}[x]
        game = create_game('test game step player', 0)
        game.tickets = "{'misterx':{'login0':''}, 'detectives':{'login3': 'car_secret', 'login':'train_addition,plane_addition,,,train_double'}}"
        game.queue = 'detectives'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives')

        res = {'res_tickets':'train_addition,plane_addition,,,train_double'}
        self.assertEqual(JsonResponse(res).content, remove_tickets(request, game.pk).content)

    def test_remove_tickets_4(self):    
        request = MagicMock()
        request.method = 'POST'
        request.user = self.user
        request.POST.get = lambda x, _: {'tickets': '0+train_addition,1+plane_addition,4+train_double'}[x]
        game = create_game('test game step player', 0)
        game.tickets = "{'misterx':{'login0':''}, 'detectives':{'login3': 'car_secret', 'login':'train_addition,plane_addition,,,train_double'}}"
        game.queue = 'detectives'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives')

        res = {'res_tickets':',,,,'}
        self.assertEqual(JsonResponse(res).content, remove_tickets(request, game.pk).content)

    def test_remove_tickets_5(self):    
        request = MagicMock()
        request.method = 'POST'
        request.user = self.user
        request.POST.get = lambda x, _: {'tickets': '0+car_addition,1+plane_addition,4+train_double'}[x]
        game = create_game('test game step player', 0)
        game.tickets = "{'misterx':{'login0':''}, 'detectives':{'login3': 'car_secret', 'login':'train_addition,plane_addition,,,train_double'}}"
        game.queue = 'detectives'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives')

        res = {'res_tickets':'train_addition,plane_addition,,,train_double'}
        self.assertEqual(JsonResponse(res).content, remove_tickets(request, game.pk).content)

    @patch('games.generation.generate_one_ticket', return_value='car_secret')
    def test_complite_course_1(self, super_generate_one_tickets):
        request = MagicMock()
        request.method = 'GET'
        request.user = self.user
        game = create_game('test game step player', 0)
        game.tickets = "{'misterx':{'login0':''}, 'detectives':{'login3': 'car_secret', 'login':'train_addition,plane_addition,,,train_double'}}"
        game.tickets_for_special_arts = "{'misterx':{'login0':''}, 'detectives':{'login': ''}}"
        game.queue = 'detectives'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives')

        res = {'tickets':'train_addition,plane_addition,car_secret,car_secret,train_double,car_secret,car_secret,car_secret,car_secret'}
        self.assertEqual(JsonResponse(res).content, complite_course(request, game.pk).content)

    @patch('games.generation.generate_one_ticket', return_value='car_secret')
    def test_complite_course_2(self, super_generate_one_tickets):
        request = MagicMock()
        request.method = 'GET'
        request.user = self.user
        game = create_game('test game step player', 0)
        game.tickets = "{'detectives':{'login':'train_addition,plane_addition,car_secret,car_secret,train_double,car_secret,car_secret,car_secret,car_secret'}}"
        game.tickets_for_special_arts = "{'misterx':{'login0':''}, 'detectives':{'login': ''}}"
        game.queue = 'detectives'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives')

        res = {'tickets':'train_addition,plane_addition,car_secret,car_secret,train_double,car_secret,car_secret,car_secret,car_secret'}
        self.assertEqual(JsonResponse(res).content, complite_course(request, game.pk).content)

    @patch('games.generation.generate_one_ticket', return_value='car_secret')
    def test_complite_course_3(self, super_generate_one_tickets):
        request = MagicMock()
        request.method = 'GET'
        request.user = self.user
        game = create_game('test game step player', 0)
        game.tickets = "{'detectives':{'login':''}}"
        game.tickets_for_special_arts = "{'misterx':{'login0':''}, 'detectives':{'login': ''}}"
        game.queue = 'detectives'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives')

        res = {'tickets':','.join(['car_secret']*9)}
        self.assertEqual(JsonResponse(res).content, complite_course(request, game.pk).content)

    @patch('games.generation.generate_one_ticket', return_value='car_secret')
    def test_complite_course_4(self, super_generate_one_tickets):
        request = MagicMock()
        request.method = 'GET'
        request.user = self.user
        game = create_game('test game step player', 0)
        game.tickets = "{'detectives':{'login':''}}"
        game.tickets_for_special_arts = "{'misterx':{'login0':''}, 'detectives':{'login': ''}}"
        game.queue = 'misterx'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives')

        res = {'tickets':''}
        self.assertEqual(JsonResponse(res).content, complite_course(request, game.pk).content)

    @patch('games.validate_action.validate_win_detectives', return_value=True)
    @patch('games.validate_action.validate_win_misterx', return_value=False)
    @patch('games.generation.generate_one_ticket', return_value='car_secret')
    def test_check_end_1(self, super_generate_one_tickets, super_vwd, super_vwmx):
        request = MagicMock()
        request.method = 'GET'
        request.user = self.user
        game = create_game('test game step player', 0)
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives')

        res = {'win':'detectives', 'position':'0'}
        self.assertEqual(JsonResponse(res).content, check_end(request, game.pk).content)

    @patch('games.validate_action.validate_win_detectives', return_value=False)
    @patch('games.validate_action.validate_win_misterx', return_value=True)
    @patch('games.generation.generate_one_ticket', return_value='car_secret')
    def test_check_end_2(self, super_generate_one_tickets, super_vwd, super_vwmx):
        request = MagicMock()
        request.method = 'GET'
        request.user = self.user
        game = create_game('test game step player', 0)
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives')

        res = {'win':'misterx', 'position': '0'}
        self.assertEqual(JsonResponse(res).content, check_end(request, game.pk).content)

    def test_get_evidences_1(self):
        request = MagicMock()
        request.method = 'GET'
        request.user = self.user
        game = create_game('test game', 0)
        game.city_way = "{'misterx':{'white':'1,2,3,4,5'},'detectives':{'green':'3,2','red':'23,45,23,1','orange':'123','purple':'','blue':'45,45,56,5'}}"
        game.cities = "{'misterx':{'white':'5'},'detectives':{'green':'2','red':'1','orange':'123','purple':'','blue':'5'}}"
        game.evidences = "2+3"
        game.save()

        game.add_evidences()

        res = {'evidences': ['0+1', '1+2', '2+3', '4+5']}
        self.assertEqual(JsonResponse(res).content, get_evidences(request, game.pk).content)

    def test_get_evidences_2(self):
        request = MagicMock()
        request.method = 'GET'
        request.user = self.user
        game = create_game('test game', 0)
        game.city_way = "{'misterx':{'white':'11,22,33,44,55'},'detectives':{'green':'3,2','red':'23,45,23,1','orange':'123','purple':'','blue':'45,45,56,5'}}"
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives')

        res = {'evidences': ['']}
        self.assertEqual(JsonResponse(res).content, get_evidences(request, game.pk).content)

    def test_get_color_misterx(self):
        request = MagicMock()
        request.method = 'GET'
        request.user = self.user
        game = create_game('test game', 0)
        game.color_city_misterx = 'yellow'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx')

        res = {'color_misterx': 'yellow'}
        self.assertEqual(JsonResponse(res).content, get_color_misterx(request, game.pk).content)

    def test_validate_double_step_1(self):
        request = MagicMock()
        request.method = 'POST'
        request.user = self.user
        request.POST.get = lambda x, _: {'drag':'0+car_double,1+car_double,2+car_double'}[x]
        game = create_game('test game', 0)
        game.tickets = "{'misterx':{'login':'car_double,car_double,car_double'}}"
        game.queue = 'misterx'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx')

        self.assertEqual(JsonResponse({'result':True}).content, validate_double_step(request, game.pk).content)

    def test_validate_double_step_2(self):
        request = MagicMock()
        request.method = 'POST'
        request.user = self.user
        request.POST.get = lambda x, _: {'drag':'1+car_double,2+car_double'}[x]
        game = create_game('test game', 0)
        game.tickets = "{'misterx':{'login':'car_double,car_double,car_double'}}"
        game.queue = 'misterx'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx')

        self.assertEqual(JsonResponse({'result':False}).content, validate_double_step(request, game.pk).content)

    def test_validate_double_step_3(self):
        request = MagicMock()
        request.method = 'POST'
        request.user = self.user
        request.POST.get = lambda x, _: {'drag':'0+car_double,1+car_double,2+car_double'}[x]
        game = create_game('test game', 0)
        game.tickets = "{'misterx':{'login':'car_double,car_double,car_double'}}"
        game.queue = 'detectives'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx')

        self.assertEqual(JsonResponse({'result':False}).content, validate_double_step(request, game.pk).content)
        
    def test_validate_double_step_with_secret_1(self):
        request = MagicMock()
        request.method = 'POST'
        request.user = self.user
        request.POST.get = lambda x, _: {'drag':'0+car_double,1+car_double,2+car_double,3+car_secret,4+car_secret,5+car_secret'}[x]
        game = create_game('test game', 0)
        game.tickets = "{'misterx':{'login':'car_double,car_double,car_double,car_secret,car_secret,car_secret'}}"
        game.queue = 'misterx'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx')

        self.assertEqual(JsonResponse({'result':True}).content, validate_double_step(request, game.pk).content)

    def test_validate_double_step_with_secret_2(self):
        request = MagicMock()
        request.method = 'POST'
        request.user = self.user
        request.POST.get = lambda x, _: {'drag':'0+car_double,1+car_secret,2+car_double,3+car_double,4+car_secret,5+car_secret'}[x]
        game = create_game('test game', 0)
        game.tickets = "{'misterx':{'login':'car_double,car_double,car_double,car_secret,car_secret,car_secret'}}"
        game.queue = 'misterx'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx')

        self.assertEqual(JsonResponse({'result':False}).content, validate_double_step(request, game.pk).content)

    def test_add_special_arts_1(self):
        request = MagicMock()
        request.method = 'POST'
        request.user = self.user2
        request.POST.get = lambda x, _: {'tickets':'0+plane_search,1+plane_search'}[x]
        game = create_game('test game', 0)
        game.tickets_for_special_arts = "{'misterx':{'':''},'detectives':{'login2':'','login1':'train_search'}}"
        game.tickets = "{'misterx':{''},'detectives':{'login1':'train_search,', 'login2':'plane_search,plane_search'}}"
        game.queue = 'detectives'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user2, role='detectives')

        self.assertEqual(JsonResponse({'result': True, 'tickets': ',', 'special':'plane_search,plane_search,train_search'}).content, add_special_arts(request, game.pk).content)

    def test_add_special_arts_2(self):
        request = MagicMock()
        request.method = 'POST'
        request.user = self.user2
        request.POST.get = lambda x, _: {'tickets':'0+plane_search,1+plane_search'}[x]
        game = create_game('test game', 0)
        game.tickets_for_special_arts = "{'misterx':{'':''},'detectives':{'login2':'','login1':'train_search'}}"
        game.tickets = "{'misterx':{''},'detectives':{'login1':'train_search,', 'login2':'plane_search,plane_search'}}"
        game.queue = 'misterx'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user2, role='detectives')

        self.assertEqual(JsonResponse({'result': False, 'tickets': 'plane_search,plane_search', 'special':'train_search'}).content, add_special_arts(request, game.pk).content)

    def test_add_special_arts_3(self):
        request = MagicMock()
        request.method = 'POST'
        request.user = self.user2
        request.POST.get = lambda x, _: {'tickets':'0+plane_search,1+plane_search'}[x]
        game = create_game('test game', 0)
        game.tickets_for_special_arts = "{'misterx':{'':''},'detectives':{'login2':'','login1':'train_search'}}"
        game.tickets = "{'misterx':{''},'detectives':{'login1':'train_addition,', 'login2':'plane_search,plane_search'}}"
        game.queue = 'detectives'
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user2, role='detectives')

        self.assertEqual(JsonResponse({'result': False, 'tickets': 'plane_search,plane_search', 'special':'train_addition'}).content, add_special_arts(request, game.pk).content)


class TestValidateAction(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='login', id=1)
        self.user.set_password('12345')
        self.user.save()
        self.client = Client()
        self.client.login(username='login', password='12345')

        self.user2 = User.objects.create(username='login2', id=2)
        self.user2.set_password('12345')
        self.user2.save()
        self.client.login(username='login2', password='12345')

    def test_get_cities_of_detectives(self):
        game = create_game('test_game', 0)
        cities = {'detectives': {'green': '4', 'orange': '5'}}
        game.cities = str(cities)
        self.assertEqual(set(get_cities_of_detetctives(game)), set([5, 4]))

    def test_validate_free_city(self):
        enemies_city = [1, 2, 3, 4, 5]
        self.assertEqual(validate_free_city(100, enemies_city), True)
        self.assertEqual(validate_free_city(1, enemies_city), False)

    def test_validate_connection(self):
        self.assertEqual(validate_connection(1, 123, 'train'), False)
        self.assertEqual(validate_connection(1, 2, 'car'), True)
        self.assertEqual(validate_connection(1, 2, 'plane'), False)
        self.assertEqual(validate_connection(197, 189, 'car'), True)

    @patch('games.validate_action.validate_free_city', return_value=True)
    @patch('games.validate_action.validate_connection', return_value=True)
    def test_validate_step_detective_true(self, super_validate_free_city, super_validate_connection):
        game = create_game('test_game', 0)
        game.queue = 'detectives'
        self.assertEqual(validate_step_detective(1, 2, 'car', game), True)

    @patch('games.validate_action.validate_free_city', return_value=False)
    @patch('games.validate_action.validate_connection', return_value=True)
    def test_validate_step_detective_false_city(self, super_validate_free_city, super_validate_connection):
        game = create_game('test_game', 0)
        game.queue = 'detectives'
        self.assertEqual(validate_step_detective(1, 2, 'car', game), False)

    @patch('games.validate_action.validate_free_city', return_value=False)
    @patch('games.validate_action.validate_connection', return_value=False)
    def test_validate_step_detective_false_city_connection(self, super_validate_free_city, super_validate_connection):
        game = create_game('test_game', 0)
        game.queue = 'detectives'
        self.assertEqual(validate_step_detective(1, 2, 'car', game), False)

    @patch('games.validate_action.validate_free_city', return_value=True)
    @patch('games.validate_action.validate_connection', return_value=True)
    def test_validate_step_detective_false_queue(self, super_validate_free_city, super_validate_connection):
        game = create_game('test_game', 0)
        game.queue = 'misterx'
        self.assertEqual(validate_step_detective(1, 2, 'car', game), False)

    @patch('games.validate_action.validate_connection', return_value=True)
    def test_validate_step_misterx_true(self, super_validate_connection):
        game = create_game('test_game', 0)
        game.queue = 'misterx'
        self.assertEqual(validate_step_misterx(1, 2, 'car', game), True)

    @patch('games.validate_action.validate_connection', return_value=False)
    def test_validate_step_misterx_false_connection(self, super_validate_connection):
        game = create_game('test_game', 0)
        game.queue = 'misterx'
        self.assertEqual(validate_step_misterx(1, 2, 'car', game), False)

    @patch('games.validate_action.validate_connection', return_value=True)
    def test_validate_step_misterx_false_queue(self, super_validate_connection):
        game = create_game('test_game', 0)
        game.queue = 'detectives'
        self.assertEqual(validate_step_misterx(1, 2, 'car', game), False)

    @patch('games.validate_action.validate_step_detective', return_value=True)
    @patch('games.validate_action.validate_step_misterx', return_value=False)
    def test_validate_step_player_detective_true(self, super_validate_step_detective, super_validate_step_misterx):
        game = create_game('test_game', 0)
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives')
        self.assertEqual(validate_step_player(1, 2, 'car', game, ging, 'green'), True)

    @patch('games.validate_action.validate_step_detective', return_value=False)
    @patch('games.validate_action.validate_step_misterx', return_value=False)
    def test_validate_step_player_detective_false(self, super_validate_step_detective, super_validate_step_misterx):
        game = create_game('test_game', 0)
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives')
        self.assertEqual(validate_step_player(1, 2, 'car', game, ging, 'green'), False)

    @patch('games.validate_action.validate_step_detective', return_value=False)
    @patch('games.validate_action.validate_step_misterx', return_value=False)
    def test_validate_step_player_misterx_false(self, super_validate_step_detective, super_validate_step_misterx):
        game = create_game('test_game', 0)
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx', colors="['white']")
        self.assertEqual(validate_step_player(1, 2, 'car', game, ging, 'white'), False)

    @patch('games.validate_action.validate_step_detective', return_value=False)
    @patch('games.validate_action.validate_step_misterx', return_value=True)
    def test_validate_step_player_misterx_true(self, super_validate_step_detective, super_validate_step_misterx):
        game = create_game('test_game', 0)
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx', colors="['white']")
        self.assertEqual(validate_step_player(1, 2, 'car', game, ging, 'white'), True)

    @patch('games.validate_action.validate_step_detective', return_value=True)
    @patch('games.validate_action.validate_step_misterx', return_value=True)
    def test_validate_step_player_detective_used_false(self, super_validate_step_detective, super_validate_step_misterx):
        game = create_game('test_game', 0)
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx', used='green')
        self.assertEqual(validate_step_player(1, 2, 'car', game, ging, 'green'), False)

    @patch('games.validate_action.validate_step_detective', return_value=True)
    @patch('games.validate_action.validate_step_misterx', return_value=True)
    def test_validate_step_player_misterx_colors(self, super_validate_step_detective, super_validate_step_misterx):
        game = create_game('test_game', 0)
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx', colors="['white']")
        self.assertEqual(validate_step_player(1, 2, 'car', game, ging, 'green'), False)

    @patch('games.validate_action.validate_step_detective', return_value=True)
    @patch('games.validate_action.validate_step_misterx', return_value=True)
    def test_validate_step_player_detective_colors(self, super_validate_step_detective, super_validate_step_misterx):
        game = create_game('test_game', 0)
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives', colors="['red', 'orange']")
        self.assertEqual(validate_step_player(1, 2, 'car', game, ging, 'green'), False)

    def test_validate_win_misterx_true(self):
        game = create_game('test game', 0)
        game.color_city_misterx = 'yellow'
        game.city_way = "{'misterx':{'white':'1,2,6,7'}}"
        game.save()
        self.assertEqual(validate_win_misterx(game), True)

    def test_validate_win_misterx_false(self):
        game = create_game('test game', 0)
        game.color_city_misterx = 'yellow'
        game.city_way = "{'misterx':{'white':'34,45,190'}}"
        self.assertEqual(validate_win_misterx(game), False)

    def test_validate_win_detectives_true(self):
        game = create_game('test game', 0)
        game.cities = "{'misterx':{'white':'190'},'detectives':{'green':'190','blue':'34'}}"
        self.assertEqual(validate_win_detectives(game), True)

    def test_validate_win_detectives_false(self):
        game = create_game('test game', 0)
        game.cities = "{'misterx':{'white':'191'},'detectives':{'green':'190','blue':'34'}}"

        self.assertEqual(validate_win_detectives(game), False)

    def test_validate_del_tickets_1(self):
        game = create_game('test game', 0)
        game.queue = 'detectives'
        game.tickets = "{'misterx':{'login1':''}, 'detectives':{'login':'car_secret,train_double,car_double'}}"
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives', colors='["green"]')
        ging.save()
        self.assertEqual(validate_del_tickets(['car_secret', 'car_double'], game, ging, 'login'), True)

    def test_validate_del_tickets_2(self):
        game = create_game('test game', 0)
        game.queue = 'detectives'
        game.tickets = "{'misterx':{'login1':''}, 'detectives':{'login':'car_secret,train_double,car_double'}}"
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives', colors='["green"]')
        ging.save()
        self.assertEqual(validate_del_tickets(['train_secret', 'car_double'], game, ging, 'login'), False)

    def test_validate_del_tickets_3(self):
        game = create_game('test game', 0)
        game.queue = 'misterx'
        game.tickets = "{'misterx':{'login1':''}, 'detectives':{'login':'car_secret,train_double,car_double'}}"
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives', colors='["green"]')
        ging.save()
        self.assertEqual(validate_del_tickets(['car_secret', 'car_double'], game, ging, 'login'), False)

    def test_validate_del_tickets_4(self):
        game = create_game('test game', 0)
        game.queue = 'detectives'
        game.tickets = "{'misterx':{'login1':''}, 'detectives':{'login':'car_secret,train_double,car_double'}}"
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives', colors='["green"]')
        ging.save()
        self.assertEqual(validate_del_tickets(['car_secret'], game, ging, 'login'), True)

    def test_validate_del_tickets_5(self):
        game = create_game('test game', 0)
        game.queue = 'detectives'
        game.tickets = "{'misterx':{'login1':''}, 'detectives':{'login':'car_secret,train_double,car_double'}}"
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives', colors='["green"]')
        ging.save()
        self.assertEqual(validate_del_tickets(['car_secret', 'car_secret','car_secret'], game, ging, 'login'), False)

    def test_validate_tickets_1(self):
        game = create_game('test game', 0)
        game.tickets = "{'misterx':{'login1':''}, 'detectives':{'login':'car_secret,train_double,car_secret'}}"
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives', colors='["green"]')
        self.assertEqual(validate_tickets([(1, 'train_double'), (2, 'car_secret')], game, ging, 'login'), True)

    def test_validate_tickets_2(self):
        game = create_game('test game', 0)
        game.tickets = "{'misterx':{'login1':''}, 'detectives':{'login':'car_secret,train_double,car_double'}}"
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives', colors='["green"]')
        self.assertEqual(validate_tickets([(1, 'car_double'), (2, 'car_double')], game, ging, 'login'), False)

    def test_validate_tickets_3(self):
        game = create_game('test game', 0)
        game.tickets = "{'misterx':{'login1':''}, 'detectives':{'login':'car_secret,train_double,car_double'}}"
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives', colors='["green"]')
        self.assertEqual(validate_tickets([(1, 'plane_double'), (2, 'car_double')], game, ging, 'login'), False)

    def test_validate_tickets_4(self):
        game = create_game('test game', 0)
        game.tickets = "{'misterx':{'login1':''}, 'detectives':{'login':'car_secret,train_double,car_double'}}"
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives', colors='["green"]')
        self.assertEqual(validate_tickets([(0, 'car_double'), (2, 'car_double'), (0, 'car_double')], game, ging, 'login'), False)

    @patch('games.validate_action.validate_step_misterx', return_value=True)
    def test_validate_special_art_secret_step_1(self, super_vwm):
        game = create_game('test game', 0)
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx', colors='["white"]')
        self.assertEqual(True, 
                validate_special_art_secret_step(1, 2, 'car', [(i, 'car_secret') for i in range(3)], game, ging, 'white')
                )

    @patch('games.validate_action.validate_step_misterx', return_value=False)
    def test_validate_special_art_secret_step_2(self, super_vwm):
        game = create_game('test game', 0)
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx', colors='["white"]')
        self.assertEqual(False, 
                validate_special_art_secret_step(1, 2, 'car', [(i, 'car_secret') for i in range(3)], game, ging, 'white')
                )

    @patch('games.validate_action.validate_step_misterx', return_value=True)
    def test_validate_special_art_secret_step_3(self, super_vwm):
        game = create_game('test game', 0)
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx', colors='["white"]')
        self.assertEqual(False, 
                validate_special_art_secret_step(1, 2, 'car', [(i, 'car_double') for i in range(3)], game, ging, 'white')
                )

    @patch('games.validate_action.validate_step_misterx', return_value=True)
    def test_validate_special_art_secret_step_4(self, super_vwm):
        game = create_game('test game', 0)
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives', colors='["green"]')
        self.assertEqual(False, 
                validate_special_art_secret_step(1, 2, 'car', [(i, 'car_secret') for i in range(3)], game, ging, 'white')
                )

    @patch('games.validate_action.validate_step_misterx', return_value=True)
    def test_validate_special_art_secret_step_5(self, super_vwm):
        game = create_game('test game', 0)
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx', colors='["white"]', used="white")
        self.assertEqual(False, 
                validate_special_art_secret_step(1, 2, 'car', [(i, 'car_secret') for i in range(3)], game, ging, 'white')
                )

    @patch('games.validate_action.validate_step_misterx', return_value=True)
    def test_validate_special_art_double_step_1(self, super_vwm):
        game = create_game('test game', 0)
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx', colors='["white"]')
        self.assertEqual(True, 
                validate_special_art_double_step(1, 2, 3, ['car', 'train'], [(i, 'car_double') for i in range(3)], game, ging, 'white')
                )

    @patch('games.validate_action.validate_step_misterx', return_value=False)
    def test_validate_special_art_double_step_2(self, super_vwm):
        game = create_game('test game', 0)
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx', colors='["white"]')
        self.assertEqual(False, 
                validate_special_art_double_step(1, 2, 3, ['car', 'train'], [(i, 'car_double') for i in range(3)], game, ging, 'white')
                )

    @patch('games.validate_action.validate_step_misterx', return_value=True)
    def test_validate_special_art_double_step_3(self, super_vwm):
        game = create_game('test game', 0)
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx', colors='["white"]')
        self.assertEqual(False, 
                validate_special_art_double_step(1, 2, 3, ['car', 'train'], [(i, 'car_secret') for i in range(3)], game, ging, 'white')
                )

    @patch('games.validate_action.validate_step_misterx', return_value=True)
    def test_validate_special_art_double_step_4(self, super_vwm):
        game = create_game('test game', 0)
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives', colors='["green"]')
        self.assertEqual(False, 
                validate_special_art_double_step(1, 2, 3, ['car', 'train'], [(i, 'car_double') for i in range(3)], game, ging, 'white')
                )

    @patch('games.validate_action.validate_step_misterx', return_value=True)
    def test_validate_special_art_double_step_5(self, super_vwm):
        game = create_game('test game', 0)
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx', colors='["white"]', used="white")
        self.assertEqual(False, 
                validate_special_art_double_step(1, 2, 3, ['car', 'train'], [(i, 'car_double') for i in range(3)], game, ging, 'white')
                )


class TestGenerate(TestCase):
    @patch('random.randint', return_value=8)
    def test_generate_one_ticket_1(self, super_randint):
        self.assertEqual(generation.generate_one_ticket(), 'car_double')

    @patch('random.randint', return_value=119)
    def test_generate_one_ticket_2(self, super_randint):
        self.assertEqual(generation.generate_one_ticket(), 'train_secret')

    @patch('random.randint', return_value=100)
    def test_generate_one_ticket_4(self, super_randint):
        self.assertEqual(generation.generate_one_ticket(), 'train_search')

    @patch('random.randint', return_value=0)
    def test_generate_one_ticket_3(self, super_randint):
        self.assertEqual(generation.generate_one_ticket(), 'car_addition')

    def test_generate_new_tickets(self):
        self.assertEqual(len(generation.generate_new_tickets(3)), 3)

    def test_generate_cities_for_detectives(self):
        self.assertEqual(len(generation.generate_cities_for_detectives()), 5)

    def test_generate_cities_for_detectives_2(self):
        self.assertEqual(len(set(generation.generate_cities_for_detectives())), 5)

    def test_generate_city_for_misterx(self):
        self.assertEqual(type(generation.generate_city_for_misterx('green')), type(int(1)))


class TestSteps(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='login', id=1)
        self.user.set_password('12345')
        self.user.save()
        self.client = Client()
        self.client.login(username='login', password='12345')

        self.user2 = User.objects.create(username='login2', id=2)
        self.user2.set_password('12345')
        self.user2.save()
        self.client.login(username='login2', password='12345')

    def test_get_type_of_step_1(self):
        drag = []
        addition_tickets = '' 
        self.assertEqual(steps.get_type_of_step(drag, addition_tickets, 'misterx'), (False, None))

    def test_get_type_of_step_2(self):
        drag = [(1, 'car_secret')]
        addition_tickets = '' 
        self.assertEqual(steps.get_type_of_step(drag, addition_tickets, 'misterx'), ('normal step', None))

    def test_get_type_of_step_3(self):
        drag = [(1, 'car_secret')]
        addition_tickets = [(0, 'car_secret'), (2, 'car_secret'), (3, 'car_secret')] 
        self.assertEqual(steps.get_type_of_step(drag, addition_tickets, 'misterx'), ('secret step', None))

    def test_get_type_of_step_4(self):
        drag = [(1, 'car_secret'), (4, 'plane_search')]
        addition_tickets = [(0, 'car_double'), (2, 'car_double'), (3, 'car_double')] 
        self.assertEqual(steps.get_type_of_step(drag, addition_tickets, 'misterx'), ('double step', None))

    def test_get_type_of_step_5(self):
        drag = ''
        addition_tickets = [(0, 'car_search'), (2, 'car_search'), (3, 'car_search')] 
        self.assertEqual(steps.get_type_of_step(drag, addition_tickets, 'detectives'), ('search step', 4))

    def test_get_type_of_step_6(self):
        drag = ''
        addition_tickets = [(0, 'car_addition'), (2, 'car_addition'), (3, 'car_addition')] 
        self.assertEqual(steps.get_type_of_step(drag, addition_tickets, 'detectives'), ('addition step', 'car'))

    def test_get_type_of_step_7(self):
        drag = [(1, 'car_secret'), (4, 'plane_search')]
        addition_tickets = [(0, 'car_secret'), (2, 'car_secret'), (3, 'car_secret'), (1, 'car_double'), (4, 'car_double'), (5, 'car_double')] 
        self.assertEqual(steps.get_type_of_step(drag, addition_tickets, 'misterx'), ('secret double step', None))

    def test_get_type_of_step_8(self):
        drag = [(1, 'car_secret'), (4, 'plane_search')]
        addition_tickets = [(0, 'car_double'), (2, 'car_double'), (3, 'car_double'), (1, 'car_secret'), (4, 'car_secret'), (5, 'car_secret')] 
        self.assertEqual(steps.get_type_of_step(drag, addition_tickets, 'misterx'), ('double secret step', None))

    @patch('games.validate_action.validate_step_player', return_value=True)
    def test_normal_step_1(self, super_va):
        game = create_game('game', 0) 
        game.cities = "{'misterx':{'white':'1'},'detectives':{'green':'2'}}"
        game.tickets = "{'misterx':{'white':''},'detectives':{'login':'plane_secret,car_double'}}"
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives', colors='["green"]', used="")
        
        data = {'result': True, 'res_city':2, 'res_tickets': 'plane_secret,'}
        self.assertEqual(data,
                steps.normal_step(1, 2, (1, 'car_double'), game, ging, 'green', 'login')
                )
        self.assertEqual({'green'}, ging.get_used)
        self.assertEqual('car_double', game.get_used_tickets[ging.role]['green'])

    @patch('games.validate_action.validate_step_player', return_value=False)
    def test_normal_step_2(self, super_va):
        game = create_game('game', 0) 
        game.cities = "{'misterx':{'white':'1'},'detectives':{'green':'2'}}"
        game.tickets = "{'misterx':{'white':''},'detectives':{'login':'plane_secret,car_double'}}"
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='detectives', colors='["green"]', used="")
        
        data = {'result': False, 'res_city':1, 'res_tickets': 'plane_secret,car_double'}
        self.assertEqual(data,
                steps.normal_step(1, 2, (1, 'car_double'), game, ging, 'green', 'login')
                )
        self.assertEqual({''}, ging.get_used)
        self.assertEqual('', game.get_used_tickets[ging.role]['green'])

    @patch('games.validate_action.validate_special_art_secret_step', return_value=True)
    def test_secret_step_1(self, super_va):
        game = create_game('game', 0) 
        game.cities = "{'misterx':{'white':'1'},'detectives':{'green':'2'}}"
        game.tickets = "{'misterx':{'login':'plane_secret,car_double,train_secret,car_secret'},'detectives':{'login1':''}}"
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx', colors='["white"]', used="")
        
        data = {'result': True, 'res_city':2, 'res_tickets': ',,,'}
        self.assertEqual(data,
                steps.secret_step(1, 2, (1, 'car_double'), [(0, 'plane_secret'), (2, 'train_secret'), (3, 'car_secret')], game, ging, 'white', 'login')
                )
        self.assertEqual({'white'}, ging.get_used)
        self.assertEqual('secret', game.get_used_tickets[ging.role]['white'])

    @patch('games.validate_action.validate_special_art_secret_step', return_value=False)
    def test_secret_step_2(self, super_va):
        game = create_game('game', 0) 
        game.cities = "{'misterx':{'white':'1'},'detectives':{'green':'2'}}"
        game.tickets = "{'misterx':{'login':'plane_secret,car_double,train_secret,car_secret'},'detectives':{'login1':''}}"
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx', colors='["white"]', used="")
        
        data = {'result': False, 'res_city':1, 'res_tickets':'plane_secret,car_double,train_secret,car_secret'}
        self.assertEqual(data,
                steps.secret_step(1, 2, (1, 'car_double'), [(2, 'train_secret'), (3, 'car_secret')], game, ging, 'white', 'login')
                )
        self.assertEqual({''}, ging.get_used)
        self.assertEqual('', game.get_used_tickets[ging.role]['white'])

    @patch('games.validate_action.validate_special_art_double_step', return_value=True)
    def test_double_step_1(self, super_va):
        game = create_game('game', 0) 
        game.cities = "{'misterx':{'white':'1'},'detectives':{'green':'2'}}"
        game.tickets = "{'misterx':{'login':'plane_secret,car_double,train_double,car_double,car_secret'},'detectives':{'login1':''}}"
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx', colors='["white"]', used="")
        
        data = {'result': True, 'res_city':3, 'res_tickets': ',,,,'}
        self.assertEqual(data,
                steps.double_step(1, 2, 3, [(0, 'plane_secret'), (4, 'car_secret')], [(1, 'car_double'), (2, 'train_double'), (3, 'car_double')], game, ging, 'white', 'login')
                )
        self.assertEqual({'white'}, ging.get_used)
        self.assertEqual('plane_secret,car_secret', game.get_used_tickets[ging.role]['white'])

    @patch('games.validate_action.validate_special_art_double_step', return_value=False)
    def test_double_step_2(self, super_va):
        game = create_game('game', 0) 
        game.cities = "{'misterx':{'white':'1'},'detectives':{'green':'2'}}"
        game.tickets = "{'misterx':{'login':'plane_secret,car_double,train_double,car_double,car_secret'},'detectives':{'login1':''}}"
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx', colors='["white"]', used="")
        
        data = {'result': False, 'res_city':1, 'res_tickets':'plane_secret,car_double,train_double,car_double,car_secret'}
        self.assertEqual(data,
                steps.double_step(1, 2, 3, [(0, 'plane_secret'), (4, 'car_secret')], [(1, 'car_double'), (2, 'train_double'), (3, 'car_double')], game, ging, 'white', 'login')
                )
        self.assertEqual({''}, ging.get_used)
        self.assertEqual('', game.get_used_tickets[ging.role]['white'])

    @patch('games.validate_action.validate_special_art_double_step', return_value=True)
    def test_double_and_secret_step_1(self, super_va):
        game = create_game('game', 0) 
        game.cities = "{'misterx':{'white':'1'},'detectives':{'green':'5'}}"
        game.tickets = "{'misterx':{'login':'plane_search,plane_search,car_double,train_double,car_double,car_secret,train_secret,train_secret'},'detectives':{'login1':''}}"
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx', colors='["white"]', used="")
        
        data = {'result': True, 'res_city':3, 'res_tickets': ',,,,,,,'}
        self.assertEqual(data,
                steps.double_and_secret_step(1, 2, 3, [(0, 'plane_search'), (1, 'plane_search')], [(2, 'car_double'), (3, 'train_double'), (4, 'car_double'), (5, 'train_secret'), (6, 'train_secret'), (7, 'train_secret')], 'double secret step', game, ging, 'white', 'login')
                )
        self.assertEqual({'white'}, ging.get_used)
        self.assertEqual('plane_search,secret', game.get_used_tickets[ging.role]['white'])

    @patch('games.validate_action.validate_special_art_double_step', return_value=True)
    def test_double_and_secret_step_2(self, super_va):
        game = create_game('game', 0) 
        game.cities = "{'misterx':{'white':'1'},'detectives':{'green':'5'}}"
        game.tickets = "{'misterx':{'login':'plane_search,plane_search,car_double,train_double,car_double,car_secret,train_secret,train_secret'},'detectives':{'login1':''}}"
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx', colors='["white"]', used="")
        
        data = {'result': True, 'res_city':3, 'res_tickets': ',,,,,,,'}
        self.assertEqual(data,
                steps.double_and_secret_step(1, 2, 3, [(0, 'plane_search'), (1, 'plane_search')], [(5, 'train_secret'), (6, 'train_secret'), (7, 'train_secret'), (2, 'car_double'), (3, 'train_double'), (4, 'car_double')], 'secret double step', game, ging, 'white', 'login')
                )
        self.assertEqual({'white'}, ging.get_used)
        self.assertEqual('secret,plane_search', game.get_used_tickets[ging.role]['white'])

    @patch('games.validate_action.validate_special_art_double_step', return_value=False)
    def test_double_and_secret_step_3(self, super_va):
        game = create_game('game', 0) 
        game.cities = "{'misterx':{'white':'1'},'detectives':{'green':'5'}}"
        game.tickets = "{'misterx':{'login':'plane_search,plane_search,car_double,train_double,car_double,car_secret,train_secret,train_secret'},'detectives':{'login1':''}}"
        game.save()
        ging = GamersInGame.objects.create(game=game, gamer=self.user, role='misterx', colors='["white"]', used="")
        
        data = {'result': False, 'res_city':1, 'res_tickets': 'plane_search,plane_search,car_double,train_double,car_double,car_secret,train_secret,train_secret'}
        self.assertEqual(data,
                steps.double_and_secret_step(1, 2, 3, [(0, 'plane_search'), (1, 'plane_search')], [(5, 'train_secret'), (6, 'train_secret'), (7, 'train_secret'), (2, 'car_double'), (3, 'train_double'), (4, 'car_double')], 'secret double step', game, ging, 'white', 'login')
                )
        self.assertEqual({''}, ging.get_used)
        self.assertEqual('', game.get_used_tickets[ging.role]['white'])

