from . import data
import random


def generate_one_ticket():
    num = random.randint(0, 119)
    s = 0
    for key in sorted(data.count_tickets.keys()):
        s += data.count_tickets[key]
        if num < s:
            return key


def generate_new_tickets(count):
    res = []
    for _ in range(count):
        res.append(generate_one_ticket())
    return res


def generate_cities_for_detectives():
    res = []
    cities = data.start_cities_detectives
    for _ in range(5):  # 5 = count of detectives
        num = random.randint(0, len(cities)-1)
        res.append(cities[num])
        del cities[num]
    return res


def generate_city_for_misterx(color):
    '''
        return number
    '''
    cities = data.start_cities_misterx['start'][color]
    res = cities[random.randint(0, len(cities)-1)]
    return res
