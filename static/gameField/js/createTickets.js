function createTickets(){//создает все билеты
    let app = document.canvasTickets;
    var tickets = getPlayerTickets(); // getPlayerTickets - запрос с главной страницы

    app.texture = resources[nameTextureTickets].textures;
    app.tickets = [];
    app.dragTickets = [];
    app.deletedTickets = [];

    for (var i=0; i<tickets.length; i++) {
        createTicket(i, tickets[i]);//запускает функцию создания билета от номера и типа
    }
}

function createTicket(num, type){//создает один билет от номера и типа
    var nameTicket = type + '.png';
    let app = document.canvasTickets;
    var ticket = new Sprite(app.texture[nameTicket]);
    //Настройка билета
    scale = ticketScale;
    size = standartWidths['tickets'] * scale;

    ticket.width = size;
    ticket.height = 2*size;
    ticket.position.set(5 * (num+1) + size *(num), 5);
    ticket.type = type;
    ticket.number = num;
    ticket.name = num + '+' + type;

    ticket.interactive = true;
    ticket.buttonMode = true;
    ticket
        .on('mousedown', onDragStartTicket)//нажать
        .on('mouseup', onDragEndTicket);//отпустить

    app.stage.addChild(ticket);
    if (app.tickets.length < document.countTickets) {
        app.tickets.push(ticket);
    }
    else {
        app.tickets[num] = ticket;
    }
}
