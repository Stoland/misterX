function deleteCheckingTickets(){//Удаляет выбранные билеты
    let app = document.canvasTickets;
    new_tickets = removeTickets(app.dragTickets);
    console.log(new_tickets);
    
    for (var i=0; i<app.tickets.length; i++){
        if (app.tickets[i] != undefined && new_tickets[i] == '') {
            delTicket(i);
        }
    }
    // zeroCountAllArts()
}

function delTicket(num){//удаляет билет app.tickets[num]
    let app = document.canvasTickets;
    let ticket = app.tickets[num];

    ticket.alpha = 1;
    if (app.dragTickets.indexOf(ticket.number + '+' + ticket.type) != -1) {
        app.dragTickets.splice(app.dragTickets.indexOf(ticket.number + '+' + ticket.type), 1);
    }
    app.stage.removeChild(ticket);
    delete app.tickets[num];
    // minusCountArt(ticket.type.split('_')[1]);

}

function updateTickets(tickets) {
    let app = document.canvasTickets;
    for (var i=0; i<tickets.length; i++) {
        if (app.tickets[i] == undefined) {
            createTicket(i, tickets[i]); // from createTickets.js Создаем на его месте новый
        }
        else if (app.tickets[i].type != tickets[i]) {
            delTicket(i);
            createTicket(i, tickets[i]);
        }
    }
    
}

// This is the end of the course
function remakeTickets() {
    console.log('remake tickets');
    tickets = compliteCourse();
    console.log('remake', tickets)
    updateTickets(tickets);
    clearHelp();
}
