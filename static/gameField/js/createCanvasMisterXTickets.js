function createCanvasMisterXTickets(){
    let app = new Application({
	width: 256,         // default: 800
	height: 256,        // default: 600
	antialias: true,    // default: false
	transparent: false, // default: false
	resolution: 1       // default: 1
    }
    );
    // Получаем родительский div и меняем размеры
    div = document.getElementById("stepsOfMisterx");
    scale = ticketScale;
    size = standartWidths['ticketMX'] * scale;

    // Настройка полотна
    app.steps = []

    app.renderer.autoResize = true;
    app.renderer.resize(stepsFieldSize['x'], stepsFieldSize['y'] - 24);
    app.renderer.backgroundColor = 0x777777;
    div.appendChild(app.view);
    document.canvasMisterXTickets = app;

    // Подключение текстур
    app.texture = resources[nameTextureTickets].textures;
    // Создание клеток для билетов
    for (i=0; i<24; i+=1){
	let rectangle = new PIXI.Graphics();

	rectangle.lineStyle(1, 0x333333, 1);
	rectangle.beginFill(0x999999);
	rectangle.drawRect(0, 0, 2*size, size);
	rectangle.endFill();
	rectangle.x = (i % 3) * (2*size + 10) + 5;
	rectangle.y = Math.trunc(i / 3) * 4/3 *(size + 5) + 20*scale;
	app.stage.addChild(rectangle);

	let number = new PIXI.Graphics();
	number.lineStyle(1, 0x333333, 1);
	number.beginFill(0x999999);
	number.drawRect(0, 0, 25*scale, 12*scale);
	number.endFill();
	number.x = (i % 3) * (2*size + 10) + 5;
	number.y = Math.trunc(i / 3) * 4/3 * (size + 5) +10*scale;
	app.stage.addChild(number);

	color = "white";
	if (i == 12) {color = 'green'}

	let style = new PIXI.TextStyle({
	    fontFamily: "Times New Roman",
	    fontSize: Math.trunc(10*scale),
	    fill: color,
	});

	let message = new PIXI.Text(Math.trunc(i/3) + 8*(i%3) +1, style);

	message.position.set(number.x+4*scale, number.y + 1*scale - 2);
	app.stage.addChild(message);

    }

    console.log('canvas mx tickets created');
}
