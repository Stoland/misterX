loader
    .add([nameTextureDetectives, nameTextureTickets, nameTextureArts, nameImgMap])
    .on("progress", loadProgressHandler)
    .load(setup);


function loadProgressHandler() {
    console.info("loading");
}
function setup(){
    console.info("MAIN SETUP;", ' ROLE', document.roleGamer);
    createCanvasMap();
    // createCanvasArt();
    createCanvasMisterXTickets();
    createCanvasTickets();
    createTickets();

    createButtonsArt();
    document.doubleStep = false;
    document.additionStep = false;
    createCanvasSpecialArt();

    resizeFields();
    updateMisterXTickets();
    document.getElementById('evidencesField').evidences = [];
    document.getElementById('evidencesField').evidencesTR = [];
    updateEvidences();

    poll();
}
    
