function onDragStartPlayer(event) {//при нажатии мышкой на изображение детектива
    document.canvasMap
    this.data = event.data;
    this.alpha -= 0.3;
    this.dragging = true;
    this.click = true;
}

function onDragEndPlayer() {
    //привязка к городу
    if (this.click == true){
	var playerPos = this.data.getLocalPosition(this.parent);
	var playerCity = this.numCity;
	var pos = attachmentToCity(playerPos);

	console.info("Player's position ?", pos['num'], cities[pos['num']-1]);

	if (document.doubleStep == true) {
	    var testPath = testPathCity(pos['num'], playerCity);//from testToMove.js
	    var tickets = document.canvasTickets.dragTickets;
	    console.log('doubleStep', tickets, testPath['type'])

	    if (testPath['test'] == true && tickets.length == 1 &&
		testPath['type'].filter(function (x){ x == tickets[0].split('+')[1].split('_')[0]} )) {
		addDoubleStep(tickets[0], pos['num']);
		this.position.set(pos['x'], pos['y']);
		this.numCity = pos['num'];
		while (document.canvasTickets.dragTickets.length != 0) {
		    delTicket(document.canvasTickets.dragTickets[0].split('+')[0]);
		}
	    }

	    if (document.doubleStepCount == 2) {
		var testServer = checkMovePlayer(document.doublePath,
		    document.doubleTickets, this.type, document.specialArt); //from gameField.html
		console.info("Test to move gamer",
		    'test -', testServer);

		var num = testServer['res_city'];
		this.position.set(cities[num-1]['x'] * playerCoordScale, cities[num-1]['y'] * playerCoordScale);
		this.numCity = num;
		updateTickets(testServer['res_tickets'].split(','));

		document.doubleStep = false;
		document.specialArt = [];
		clearHelp();
	    }
	    else {
		var num = this.numCity;
		this.position.set(cities[num-1]['x'] * playerCoordScale, cities[num-1]['y'] * playerCoordScale);
	    }
	}
	
	else if (document.additionStep != false) {
	    var testPath = testPathCity(pos['num'], playerCity);//from testToMove.js
	    console.log(testPath, document.additionStep)
	    var testServer = checkMovePlayer([playerCity, pos['num']],
		[], this.type, document.specialArt); //from gameField.html
	    console.info("Test to move gamer",
		'test -', testServer);

	    var num = testServer['res_city'];
	    this.position.set(cities[num-1]['x'] * playerCoordScale, cities[num-1]['y'] * playerCoordScale);
	    this.numCity = num;
	    updateTickets(testServer['res_tickets'].split(','));

	    document.additionStep = false;
	    document.specialArt = [];
	    clearHelp();
	}
	else {
	    var testServer = checkMovePlayer([playerCity, pos['num']],
		document.canvasTickets.dragTickets, this.type, document.specialArt); //from gameField.html
	    console.info("Test to move gamer",
		'test -', testServer);

	    var num = testServer['res_city'];
	    this.position.set(cities[num-1]['x'] * playerCoordScale, cities[num-1]['y'] * playerCoordScale);
	    this.numCity = num;
	    updateTickets(testServer['res_tickets'].split(','));

	    document.specialArt = [];
	    clearHelp();
	}

	this.alpha += 0.3;
	this.dragging = false;
	this.click = false;
	this.data = null;
    }
}

function onDragMovePlayer() {//во время движения мышки
    if (this.dragging) {
	var newPosition = this.data.getLocalPosition(this.parent);
	this.x = newPosition.x;
	this.y = newPosition.y;
    }
}

