function createCanvasSpecialArt(){ 
    scale = ticketScale;
    size = standartWidths['tickets'] * scale;
    if (document.roleGamer == 'misterx') { count = 6 }
    else {count = 5}
    let app = new Application({
	width: (size + 5) * count + 5,
	height: 2*(size + 5),
	antialias: false,
	transparent: false,
	resolution: 1
    }
    );
    // connect to textures
    var id = resources[nameTextureTickets].textures;
    app.texture = id;

    app.renderer.backgroundColor = 0x777777;
    app.artTickets = [];
    app.dragTickets = [];
    app.deletedTickets = [];

    document.getElementById('canvasSpecialArt').appendChild(app.view);
    document.canvasSpecialArt = app;

    console.info('Canvas Art Tickets were created succesful');
}
