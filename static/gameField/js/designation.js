let Application = PIXI.Application,
    resources = PIXI.loader.resources,
    loader = PIXI.loader,
    Sprite = PIXI.Sprite,
    Text = PIXI.Text,
    TextStyle = PIXI.TextStyle;

var nameTextureDetectives = "/static/gameField/texture/textureDetectivesJSON.json",
    nameTextureTickets = "/static/gameField/texture/textureTicketsJSON.json",
    nameTextureArts = "/static/gameField/texture/textureArtsJSON.json",
    nameImgMap = '/static/gameField/map/mapL.png';

var standartWidths = {'ticketMX': 25, 'map': 1532, 'tickets': 40, 'players': 227},
    standartHeights = {'ticketMX': 50, 'map': 1165, 'tickets': 80, 'players': 227},
    mapFieldSize = {'x': document.getElementById('fieldOfMap').clientWidth, 'y': document.getElementById('fieldOfMap').clientWidth / standartWidths['map'] * standartHeights['map']},
    stepsFieldSize = {'x': document.getElementById("stepsOfMisterx").clientWidth, 'y': mapFieldSize['y']/2},
    additionalFieldSize = {'x': document.getElementById("additionalTabs").clientWidth, 'y': mapFieldSize['y']/2},
    ticketsFieldSize = {'x': document.getElementById("ticketsOfPlayer").clientWidth, 'y': document.getElementById("ticketsOfPlayer").clientWidth / standartWidths['tickets'] * standartHeights['tickets']},
    artsFieldSize = ticketsFieldSize;

var ticketScale = Math.trunc((stepsFieldSize['x'] - 30) / 6)/standartWidths['ticketMX'],
    mapScale = mapFieldSize['x'] / standartWidths['map'],
    playerScale = mapScale * 50 / standartWidths['players'],
    playerCoordScale = standartWidths['map'] / 7657 * mapScale;


getPlayerRole(); //Получение роли игрока get запросом
getQueue();
getColorMisterX();
getColors();
