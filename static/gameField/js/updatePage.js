function updateMisterXTickets() {
    let app = document.canvasMisterXTickets;
    new_steps = getSteps();
    for (var i=0; i<new_steps.length; i++) {
        if (app.steps[i] == undefined) {
            addMisterXTickets(i, new_steps[i]);
            console.log(123)
        }
    }
    console.log('success update mxtickets');
}
// function updateTickets() {
// }
function updatePositions() {
    let app = document.canvasMap;
    positions = getPositions();
    if (app.players.length == 5 && positions.length == 6) {
	// we need to add misterx after 25th step
	createPlayer('white', getPositionByNumber(positions['white']), positions['white'])
    }
    for (var i=0; i<app.players.length; i+=1) {
        player = app.players[i];
	if (player.type != 'white' && document.doubleStep != true) {
	    if (player.numCity != positions[player.type]) {
		pos = getPositionByNumber(positions[player.type]);
		player.position.set(pos[0], pos[1]);
		player.numCity = positions[player.type];
	    }
	}
    }
    console.log('success update positions', app.players);
}

function updateQueue() {
    getQueue();
}

function updateEvidences() {
    if (document.queueRole != document.roleGamer) {
	let field = document.getElementById("evidencesField");
	evidences = getEvidences();
	if (evidences != undefined && evidences != [""] && evidences != [] && evidences[0] != "") {
	    if (field.evidences != evidences) {
		createEvidences(evidences);
	    }
	}
	console.log('success update evidences');
    }
}

function standartUpdateTickets() {
    if (document.queueRole != document.roleGamer && document.roleGamer == 'detectives') {
	tickets = getPlayerTickets();
	updateTickets(tickets);
	console.log('success update tickets');
    }
}

function updatePage() {
    checkEnd();
    updateMisterXTickets();
    updatePositions();
    updateQueue();
    updateEvidences();
    standartUpdateTickets();
    // loadPaths();
    // loadChat();
    // loadEvidences();
    // loadSpecialArt();
}
