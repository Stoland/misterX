function createButtonsArt() {
    div = document.getElementById('artButtons');
    if (document.roleGamer == 'misterx') {
	inputs = '<input type="button" class="userButtons" value="Secret step" onclick=secretStep()>' + 
	    '\n' +
	    '<input type="button" class="userButtons" value="Double step" onclick=doubleStepFunc()>';
    }
    else {
	inputs = '<input type="button" class="userButtons" value="Get evidence" onclick=searchStepFunc()>' + 
	    '\n' +
	    '<input type="button" class="userButtons" value="Addition step" onclick=additionStepFunc()>';
    }
    div.innerHTML = inputs + div.innerHTML;
}

function createHelp(type, help) {
    document.getElementById('typeSpecialArt').innerHTML = type;
    document.getElementById('helpSpecialArt').innerHTML = help;
}

function clearHelp() {
    document.getElementById('typeSpecialArt').innerHTML = '';
    document.getElementById('helpSpecialArt').innerHTML = '';

    clearArts();
}

function addSpecialTicketsOnPage() {
    data = addSpecialTickets(document.canvasTickets.dragTickets);
    console.info('addSpTOP', data)
    if (data['result'] == true) {
        while (document.canvasTickets.dragTickets.length != 0) {
            delTicket(document.canvasTickets.dragTickets[0].split('+')[0]);
        }
        createArtTickets(data['special']);
        updateTickets(data['tickets']);
        document.specialArt = data['special'];
    }
}

function secretStep() {
    var count = 0;
    for (var i=0; i<document.canvasTickets.dragTickets.length; i++) {
        if (document.canvasTickets.dragTickets[i].split('_')[1] == 'secret') {
            count += 1
        }
    }
    if (count == 3 && document.queueRole == document.roleGamer && document.canvasTickets.dragTickets.length == 3) {
	document.specialArt = document.canvasTickets.dragTickets.slice();
	while (document.canvasTickets.dragTickets.length != 0) {
	    delTicket(document.canvasTickets.dragTickets[0].split('+')[0]);
	}
        createArtTickets(document.specialArt);
	createHelp('Secret step', 'Drag a ticket and move');
    }
    console.log(document.specialArt)
}

function doubleStepFunc() {
    var count = 0;
    for (var i=0; i<document.canvasTickets.dragTickets.length; i++) {
	if (document.canvasTickets.dragTickets[i].split('_')[1] == 'double') {
	    count += 1
	}
    }
    if (count == 3 && document.queueRole == document.roleGamer
        && document.doubleStep == false && validateDoubleStep(document.canvasTickets.dragTickets)) {
        document.doubleStep = true;
        document.doubleStepCount = 0;
        document.doubleTickets = [];
        startCity = document.canvasMap.players[0].numCity;
        document.doublePath = [startCity];
        document.specialArt = document.canvasTickets.dragTickets.slice();
        while (document.canvasTickets.dragTickets.length != 0) {
            delTicket(document.canvasTickets.dragTickets[0].split('+')[0]);
        }
        createArtTickets(document.specialArt);
        createHelp('Double step', 'Repeat twice: drag a ticket and move' )
    }
}

function addDoubleStep(ticket, city) {
    if (document.doubleStep == true) {
	if (document.doubleStepCount < 2) {
	    document.doubleStepCount += 1;
	    document.doubleTickets.push(ticket);
	    document.doublePath.push(city);
	}
	if (document.doubleStepCount == 2) {
	    document.doubleStep = false;
	}
    }
}

function searchStepFunc() {
    drag = document.canvasTickets.dragTickets;
    data = searchStep(drag);
    console.log('searchStepFunc', data['res_tickets'], data['result'])
    if (data['result'] == true) {
	while (document.canvasTickets.dragTickets.length != 0) {
	    delTicket(document.canvasTickets.dragTickets[0].split('+')[0]);
	}
	updateTickets(data['res_tickets'].split(','));
	createHelp('Get evidence', 'The evidence will be showing after compliting step');
    }
}

function additionStepFunc() {
    drag = document.canvasTickets.dragTickets;
    data = validateAdditionStep(drag);
    console.log('additionStepFunc', data['step'], data['result'])
    if (document.queueRole == document.roleGamer && data['result'] == true) {
	document.specialArt = document.canvasTickets.dragTickets.slice();
	document.additionStep = data['step'];
	while (document.canvasTickets.dragTickets.length != 0) {
	    delTicket(document.canvasTickets.dragTickets[0].split('+')[0]);
	}
        createArtTickets(document.specialArt);
	createHelp('Addition step', 'Move one of detectives by ' + data['step']);
    }
    console.log(document.specialArt)
}
