function addMisterXTickets(number, type) {
    let app = document.canvasMisterXTickets;
    var nameTicket = type + '.png';
    let ticket = new Sprite(app.texture[nameTicket]);

    //Настройка билета
    size = ticketScale * 25;

    // number -= 1
    ticket.type = type;
    app.steps[number] = ticket;

    ticket.width = size;
    ticket.height = 2*size;
    ticket.anchor.set(1, 0);
    ticket.rotation = -3.14/2;
    x = (Math.trunc(number/8)*2*(size+5))+5;
    y = (number % 8) * 4/3*(size+5)+20*ticketScale;
    ticket.position.set(x, y);
    
    app.stage.addChild(ticket);
    console.log('add mx ticket', number);
}

