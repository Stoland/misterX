function createPlayers(){ //Запускает создание игроков на карте
    let app = document.canvasMap;

    app.texture = resources[nameTextureDetectives].textures;
    app.players = [];
    positions = getPositions();
    console.log(positions);

    for (let color of Object.keys(positions)){
	coords = getPositionByNumber(positions[color]);
	createPlayer(color, coords, positions[color]);
    }

    console.info("Detectives was created successful");
}

function createPlayer(type, position, numCity){//Create players on map
    var namePlayer = type + '.png';
    let app = document.canvasMap;
    var player = new Sprite(app.texture[namePlayer]);

    player.scale.set(playerScale, playerScale);
    player.position.set(position[0], position[1]);
    player.type = type;

    player.numCity = numCity;
    player.interactive = true;
    player.buttonMode = true;
    player.alpha = 0.7;
    player.anchor.set(0.5);

    player
	.on('mousedown', onDragStartPlayer)
	.on('mouseup', onDragEndPlayer)
	.on('mouseupoutside', onDragEndPlayer)
	.on('mousemove', onDragMovePlayer);

    app.stage.addChild(player);
    app.players.push(player);
}

function getPositionByNumber(number) {
    city = cities.filter(function(x) { return x['num'] == number; })
    city = city[0];
    x = city['x'] * playerCoordScale; 
    y = city['y'] * playerCoordScale;
    return [x, y];
}
