function createArtTickets(tickets){//создает все билеты
    let app = document.canvasSpecialArt;

    app.texture = resources[nameTextureTickets].textures;

    // app.dragTickets = [];
    // app.deletedTickets = [];

    clearArts();
    console.log(tickets)
    for (var i=0; i<tickets.length; i++) {
        createArtTicket(i, tickets[i].split('+')[1]);//запускает функцию создания билета от номера и типа
    }
}

function clearArts() {
    let app = document.canvasSpecialArt;
    for (var i=0; i<app.artTickets.length; i++) {
        if (app.artTickets[i] != undefined) {
            app.artTickets[i].alpha = 1;
            app.stage.removeChild(app.artTickets[i]);
            delete app.artTickets[i];
        }
    }
}

function clearArtsButton() {
    tickets = getPlayerTickets();
    updateTickets(tickets);
    clearHelp();
}

function createArtTicket(num, type){//создает один билет от номера и типа
    var nameTicket = type + '.png';
    let app = document.canvasSpecialArt;
    var artTicket = new Sprite(app.texture[nameTicket]);
    //Настройка билета
    scale = ticketScale;
    size = standartWidths['tickets'] * scale;

    artTicket.width = size;
    artTicket.height = 2*size;
    artTicket.position.set(5 * (num+1) + size *(num), 5);
    artTicket.type = type;
    artTicket.number = num;
    artTicket.name = num + '+' + type;

    // ticket.interactive = true;
    // ticket.buttonMode = true;
    // ticket
    //     .on('mousedown', onDragStartTicket)//нажать
    //     .on('mouseup', onDragEndTicket);//отпустить

    app.stage.addChild(artTicket);
    if (app.artTickets.length < document.countArtTickets) {
        app.artTickets.push(artTicket);
    }
    else {
        app.artTickets[num] = artTicket;
    }
    console.log('app: ', app.artTickets)
}
