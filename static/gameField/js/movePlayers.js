function distanceToCity(pos, city){//Расстояние от позиции детектива до города 
    var dx = Math.abs(pos.x - city['x']);
    var dy = Math.abs(pos.y - city['y']);
    var d = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
    return d;
}
function attachmentToCity(position){//Должен выдавать ближайший к детективу город
    maxDist = 1000000;
    resDist = {'d':maxDist, 'x':100, 'y':100};
    //Далее находим город с минимальным расстоянием
    for (var i=0; i<cities.length; i++){//cities из файла citiesData.js
        city = cities[i];
        testCity = {'x':city['x'] * playerCoordScale, 'y':city['y'] * playerCoordScale, 'num':city['num']};
        d = distanceToCity(position, testCity);
        if (d < resDist['d']){
            resDist = {'d': d, 'x':testCity['x'], 'y': testCity['y'], 'num': testCity['num']};
        }
    }
    resDist = {'x': resDist['x'], 'y':resDist['y'],'num':resDist['num'] }
    return resDist;
}
