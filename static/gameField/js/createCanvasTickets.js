function createCanvasTickets(){ // создание canvasTickets
    scale = ticketScale;
    size = standartWidths['tickets'] * scale;
    count = getPlayerTickets().length; // getPlayerTickets - запрос с главной страницы
    let app = new Application({
	width: (size + 5) * count + 5,
	height: 2*(size + 5),
	antialias: false,
	transparent: false,
	resolution: 1
    }
    );
    // connect to textures
    var id = resources[nameTextureTickets].textures;
    app.texture = id;

    app.renderer.backgroundColor = 0x777777;
    app.tickets = [];
    app.dragTickets = [];
    app.deletedTickets = [];

    document.getElementById('ticketsOfPlayer').appendChild(app.view);
    document.canvasTickets = app;

    console.info('Canvas Tickets were created succesful');
}
