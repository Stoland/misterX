function resizeFields(){
    heightTitle = 24;
    document.getElementById('fieldOfMap').style.height = mapFieldSize['y'] + heightTitle + 'px';
    document.getElementById('stepsOfMisterx').style.height = stepsFieldSize['y'] + heightTitle/2 + 'px';
    document.getElementById('extremalAdditionalDiv').style.height = additionalFieldSize['y']+ heightTitle/2 + 'px';
    xs = document.getElementsByClassName('additionalField');
    for (i=0; i<xs.length; i++){
	xs[i].style.height = additionalFieldSize['y']-2*heightTitle + 'px';
    }
    document.getElementById('evidencesTable').style.width = additionalFieldSize['x'] -25 + 'px' ;
    
    document.getElementById('ticketsOfPlayer').style.height = 2*(standartWidths['tickets'] * ticketScale + 5) + heightTitle + 'px';
    document.getElementById('specialArts').style.height = 2*(standartWidths['tickets'] * ticketScale + 5) + heightTitle + 'px';

    xs = document.getElementsByClassName('userButtons');
    for (i=0; i<xs.length; i++){
	xs[i].style.height = (standartWidths['tickets'] * ticketScale - 1)  + 'px';
    }

    xs = document.getElementsByClassName('moveButtons');
    for (i=0; i<xs.length; i++){
	xs[i].style.height = (standartWidths['tickets'] * ticketScale - 1)  + 'px';
    }
}
