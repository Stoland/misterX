from django.shortcuts import render


def main(request):
    return render(request, 'website/page.html')

def contacts(request):
    return render(request, 'website/contacts.html')

def rules(request):
    return render(request, 'rules/Rules.html')

def short(request):
    return render(request, 'rules/shortRules.html')

def table_students(request):
    return render(request, 'website/tableStudents.html')
