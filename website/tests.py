from django.test import TestCase


class WebsitePageTest(TestCase):
    def test_page(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
