from django.urls import path, include
from django.contrib.auth import views as auth_views
from . import views

app_name = 'website'
urlpatterns = [
    path('', views.main, name='main'),
    path('contacts/', views.contacts, name='contacts'),
    path('rules/', views.rules, name='rules'),
    path('shortRules/', views.short, name='short')
]
